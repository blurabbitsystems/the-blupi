<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=Auth::user();

        if($user->people->is_admin==0)
        {
            Auth::logout();
            return redirect('/admin/login')
                ->with('status','warning')
                ->with('msg','Please login as admin.');
        }

        return $next($request);
    }
}
