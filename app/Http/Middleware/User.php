<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=Auth::user();

        if($user->people->is_admin==1)
        {
            Auth::logout();
            return redirect('/login')
                ->with('status','warning')
                ->with('msg','Please login as user.');
        }

        return $next($request);
    }
}
