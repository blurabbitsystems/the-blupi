<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\People;
use App\Models\Faculty;
use App\Models\Student;
use App\Models\Circle;
use App\Models\Department;
use App\Models\Metainfo;
use App\Models\Enrollment;
use App\Models\GeneralPost;
use App\Models\GeneralComment;
use App\Models\GeneralNotice;
use App\Models\Course;
use App\Models\Notice;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Notification;
use App\Models\Classtest;
use App\Models\Assignment;
use App\Models\Message;
use App\Models\File;
use App\Models\Submission;
use Hash;
use Auth;
use Carbon\Carbon;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['showLogin','authenticate']]);
        $this->middleware('user', ['except' => ['showLogin','authenticate']]);
    }
    
    public function showLogin()
    {
        if(!Auth::guest()) return redirect('/home');
        $metainfo=Metainfo::all();

    	return view('BluPi-View.login',compact('metainfo'));
    }

    public function authenticate(Request $request)
    {
    	$user=User::where('username',$request->username)->first();

        if($user && $user->people->is_admin)
            return redirect()->back()
                ->with('status','danger')
                ->with('msg','Username \''.$request->username.'\' is unauthorized.');

    	$credentials=
        [
            'username' => $request->username,
            'password' => $request->password
        ];

    	if(Auth::attempt($credentials,$request->rememberme)) return redirect('/home');

    	return redirect()->back()
            ->with('status','danger')
            ->with('msg','Invalid credentials.');
    }

    public function showHome()
    {
        $user=Auth::user();

        $metainfo=Metainfo::all();
        $circleList=[];

        $current_session=$metainfo->where('key','Session')->first()->value;
        $enrollmentList=$user->people->enrollments;
        foreach($enrollmentList as $enrollment)
        {
            if($enrollment->circle->session==$current_session)
                array_push($circleList,$enrollment->circle);
        }

        $notificationCount=$user->people->notifications->where('is_seen',0)->count();

        if($user->people->is_faculty==1)
        {
            $postList=GeneralPost::orderBy('created_at','desc')->simplePaginate(10);
            $noticeList=GeneralNotice::orderBy('created_at','desc')->take(5)->get();
        }
        else
        { 
            $postList=GeneralPost::where('scope_level',$user->people->Student->level)
                ->where('scope_term',$user->people->Student->term)
                ->orWhere('scope_term',0)
                ->orderby('created_at','desc')->simplePaginate(10);

            $noticeList=GeneralNotice:: where('scope_level',$user->people->Student->level)
                ->where('scope_term',$user->people->Student->term)
                ->orWhere('scope_term',0)
                ->orderby('created_at','desc')->take(5)->get();
        }

        $newMsgCount=Message::where('receiver_id',$user->id)->where('is_seen',0)->count();


        return view('BluPi-View.home',compact('user','metainfo','circleList','postList','noticeList','notificationCount','newMsgCount'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function insertGeneralPost(Request $request)
    {
        $user=Auth::user();

        $post=new GeneralPost;
        $post->people_id=$user->id;
        $post->post_body=$request->post_body;
        $post->comment_disabled=$request->comment_disabled;

        if($request->levelterm!=0)
        {
            $post->scope_level=(int)(($request->levelterm+1)/2);
            $post->scope_term=($request->levelterm-1)%2+1;
        }

        $post->save();

        $response=
        [
            'post_id'       => $post->id,
            'created_at'    => $post->created_at->setTimezone('+06:00')->format('h:i a, M d, Y'),
            'people_id'     => $user->id,
            'people_name'   => $user->people->name,
        ];

        return response()->json($response);
    }

    public function removeGeneralPost(Request $request)
    {
        GeneralPost::destroy($request->post_id);
        return response()->json();
    }

    public function insertGeneralComment(Request $request)
    {
        $user=Auth::user();

        $comment=new GeneralComment;
        $comment->people_id=$user->id;
        $comment->comment_body=$request->comment_body;
        $comment->general_post_id=$request->post_id;

        $comment->save();

        $response=
        [
            'comment_id'    => $comment->id,
            'people_id'     => $comment->people_id,
            'people_name'   => $comment->people->name,
        ];

        return response()->json($response);

    }

    public function removeGeneralComment(Request $request)
    {
        GeneralComment::destroy($request->comment_id);
        return response()->json();
    }

    public function fetchNotifications(Request $request)
    {
        $user=Auth::user();

        $notif=$user->people->notifications->where('is_seen',0);

        $notifications=[];
        foreach($notif as $n)
        {
            array_push($notifications, $n->msg);
            $n->is_seen=1;
            $n->save();
        }

        $response=
        [
            'notifications'=>$notifications,
        ];

        return response()->json($response);
    }

    public function insertGeneralNotice(Request $request)
    {
        $user=Auth::user();

        $notice=new GeneralNotice; 
        $notice->people_id=$user->id;
        $notice->subject=$request->topic;
        $notice->body=$request->description;

        if($request->levelterm!=0)
        {
            $notice->scope_level=(int)(($request->levelterm+1)/2);
            $notice->scope_term=($request->levelterm-1)%2+1;
        }

        $notice->save();

        $response=
        [
            'created_at'   => $notice->created_at->setTimezone('+06:00')->format('h:i a, M d, Y'),
            'notice_id' => $notice->id,
        ];

        return response()->json($response);

    }

    public function removeGeneralNotice(Request $request)
    {
        GeneralNotice::destroy($request->notice_id);
        return response()->json();
    }


    public function showProfile($id=0)
    {
        $user=Auth::user();
        $circleList=[];

        $metainfo=Metainfo::all();
        $current_session=$metainfo->where('key','Session')->first()->value;
        $enrollmentList=$user->people->enrollments;
        foreach($enrollmentList as $enrollment)
        {
            if($enrollment->circle->session==$current_session)
                array_push($circleList,$enrollment->circle);
        }

        $notificationCount=$user->people->notifications->where('is_seen',0)->count();

        if($id!=0) $people=People::find($id);
        else $people=$user->people;

        $newMsgCount=Message::where('receiver_id',$user->id)->where('is_seen',0)->count();

        if($user->people->is_faculty==1)
        {
            $noticeList=GeneralNotice::orderBy('created_at','desc')->take(5)->get();
        }
        else
        { 
            $noticeList=GeneralNotice:: where('scope_level',$user->people->Student->level)
                ->where('scope_term',$user->people->Student->term)
                ->orWhere('scope_term',0)
                ->orderby('created_at','desc')->take(5)->get();
        }

        return view('BluPi-View.profile',
            compact('user','circleList','notificationCount','people','newMsgCount','noticeList'));
    }

    public function editProfile()
    {
        $user=Auth::user();
        $circleList=[];

        $metainfo=Metainfo::all();
        $current_session=$metainfo->where('key','Session')->first()->value;
        $enrollmentList=$user->people->enrollments;
        foreach($enrollmentList as $enrollment)
        {
            if($enrollment->circle->session==$current_session)
                array_push($circleList,$enrollment->circle);
        }

        $notificationCount=$user->people->notifications->where('is_seen',0)->count();

        if($user->people->is_faculty==1)
        {
            $noticeList=GeneralNotice::orderBy('created_at','desc')->take(5)->get();
        }
        else
        { 
            $noticeList=GeneralNotice:: where('scope_level',$user->people->Student->level)
                ->where('scope_term',$user->people->Student->term)
                ->orWhere('scope_term',0)
                ->orderby('created_at','desc')->take(5)->get();
        }

        return view('BluPi-View.editprofile',
            compact('user','circleList','notificationCount','noticeList'));
    }

    public function saveProfile(Request $request)
    {
        $user=Auth::user();

        $file=$request->file('picture');

        if($file)
        {
            if($file->getSize()>65535)
            {
                return redirect()->back()
                    ->with('status','danger')
                    ->with('msg','File can be atmost 63 KB!.');
            }

            $user->people->picture=$file->openFile()->fread($file->getSize());
            $user->people->save();
        }

        if($user->people->is_faculty==1)
        {
            $user->people->faculty->additional_info=$request->additional_info;
            $user->people->faculty->save();
        }

        return redirect('/profile');
    }

    public function showInbox()
    {
        $user=Auth::user();
        $circleList=[];

        $metainfo=Metainfo::all();
        $current_session=$metainfo->where('key','Session')->first()->value;
        $enrollmentList=$user->people->enrollments;
        foreach($enrollmentList as $enrollment)
        {
            if($enrollment->circle->session==$current_session)
                array_push($circleList,$enrollment->circle);
        }

        $notificationCount=$user->people->notifications->where('is_seen',0)->count();


        $messageList=Message::where('people_id',$user->id)->orWhere('receiver_id',$user->id)
                        ->orderby('created_at','desc')->get();

        $newMsgCount=$messageList->where('receiver_id',$user->id)->where('is_seen',0)->count();

        if($user->people->is_faculty==1)
        {
            $noticeList=GeneralNotice::orderBy('created_at','desc')->take(5)->get();
        }
        else
        { 
            $noticeList=GeneralNotice:: where('scope_level',$user->people->Student->level)
                ->where('scope_term',$user->people->Student->term)
                ->orWhere('scope_term',0)
                ->orderby('created_at','desc')->take(5)->get();
        }

        return view('BluPi-View.inbox',
            compact('user','circleList','notificationCount','messageList','newMsgCount','noticeList'));
    }

    public function sendMessage(Request $request)
    {
        if(!$request->receiver_id)
            return redirect()->back()
            ->with('status','danger')
            ->with('msg','Person not found!.');

        $user=Auth::user();

        $message=new Message;
        $message->people_id=$user->id;
        $message->receiver_id=$request->receiver_id;
        $message->about=$request->about;
        $message->msg=$request->msg;
        $message->save();

        return redirect()->back()
            ->with('status','success')
            ->with('msg',' Message is Sent!.');
    }

    public function setMessageSeen(Request $request)
    {
        Message::destroy($request->message_id);
        return response()->json();
    }

    public function searchPeople(Request $request)
    {
        $peopleList=People::where('name','like','%'.$request->name.'%')->where('is_admin',0)->get();
        
        $ret=array();
        foreach($peopleList as $people) array_push($ret, ['id'=>$people->id, 'name'=>$people->name]);

        return $ret;
    }

    public function showCircle($id)
    {
        $user=Auth::user();
        $circle=Circle::find($id);

        $metainfo=Metainfo::all();
        $circleList=[];

        $current_session=$metainfo->where('key','Session')->first()->value;
        $enrollmentList=$user->people->enrollments;
        foreach($enrollmentList as $enrollment)
        {
            if($enrollment->circle->session==$current_session)
                array_push($circleList,$enrollment->circle);
        }

        $postList=Post::where('circle_id',$circle->id)->orderBy('created_at','desc')->simplePaginate(10);

        $notificationCount=$user->people->notifications->where('is_seen',0)->count();
        $newMsgCount=Message::where('receiver_id',$user->id)->where('is_seen',0)->count();

        return view('BluPi-View.circle',compact('user','circle','circleList','postList','notificationCount','newMsgCount'));
    }

    public function showCirclePost($id)
    {
        $user=Auth::user();
        $post=Post::find($id);

        $circle=$post->circle;

        $metainfo=Metainfo::all();
        $circleList=[];

        $current_session=$metainfo->where('key','Session')->first()->value;
        $enrollmentList=$user->people->enrollments;
        foreach($enrollmentList as $enrollment)
        {
            if($enrollment->circle->session==$current_session)
                array_push($circleList,$enrollment->circle);
        }

        $notificationCount=$user->people->notifications->where('is_seen',0)->count();
        $newMsgCount=Message::where('receiver_id',$user->id)->where('is_seen',0)->count();

        return view('BluPi-View.circlepost',compact('user','circle','circleList','post','notificationCount','newMsgCount'));
    }


    public function insertFile(Request $request)
    {
        $user=Auth::user();
        $circle=Circle::find($request->circle_id);

        $f=$request->file('file');

        $file=new File;
        $file->circle_id=$request->circle_id;
        $file->name=$f->getClientOriginalName();
        $file->size=$f->getSize();
        $file->mime=$f->getMimeType();
        $file->data=$f->openFile()->fread($f->getSize());
        $file->comment=$request->comment;
        $file->save();

        $this->insertNotification($user,$request->circle_id,'A file was uploaded in ');

        $response=
        [
            'id'   => $file->id,
            'name' => $file->name,
            'size' => $file->size
        ];

        return response()->json($response);
    }

    public function showSearchResult(Request $request)
    {
        $user=Auth::user();
        $metainfo=Metainfo::all();
                
        $circleList=[];

        $current_session=$metainfo->where('key','Session')->first()->value;
        $enrollmentList=$user->people->enrollments;
        foreach($enrollmentList as $enrollment)
        {
            if($enrollment->circle->session==$current_session)
                array_push($circleList,$enrollment->circle);
        }

        if($user->people->is_faculty==1)
        {
            $noticeList=GeneralNotice::orderBy('created_at','desc')->take(5)->get();
        }
        else
        { 
            $noticeList=GeneralNotice:: where('scope_level',$user->people->Student->level)
                ->where('scope_term',$user->people->Student->term)
                ->orWhere('scope_term',0)
                ->orderby('created_at','desc')->take(5)->get();
        }

        $notificationCount=$user->people->notifications->where('is_seen',0)->count();
        $newMsgCount=Message::where('receiver_id',$user->id)->where('is_seen',0)->count();

        $peopleResults=People::where('name','like','%'.$request->q.'%')->where('is_admin',0)->get();

        return view('BluPi-View.searchresult',
            compact('user','circleList','notificationCount','newMsgCount','noticeList','peopleResults'));
    }

    public function showGeneralPost($id)
    {
        $user=Auth::user();
        $post=GeneralPost::find($id);
        $metainfo=Metainfo::all();

        $circleList=[];

        $current_session=$metainfo->where('key','Session')->first()->value;
        $enrollmentList=$user->people->enrollments;
        foreach($enrollmentList as $enrollment)
        {
            if($enrollment->circle->session==$current_session)
                array_push($circleList,$enrollment->circle);
        }

        if($user->people->is_faculty==1)
        {
            $noticeList=GeneralNotice::orderBy('created_at','desc')->take(5)->get();
        }
        else
        { 
            $noticeList=GeneralNotice:: where('scope_level',$user->people->Student->level)
                ->where('scope_term',$user->people->Student->term)
                ->orWhere('scope_term',0)
                ->orderby('created_at','desc')->take(5)->get();
        }

        $notificationCount=$user->people->notifications->where('is_seen',0)->count();
        $newMsgCount=Message::where('receiver_id',$user->id)->where('is_seen',0)->count();
        
        return view('BluPi-View.post',
            compact('user','circleList','notificationCount','post','newMsgCount','noticeList','metainfo'));
    }

    public function insertCirclePost(Request $request)
    {
        $user=Auth::user();

        $post=new Post;
        $post->people_id=$user->id;
        $post->circle_id=$request->circle_id;
        $post->post_body=$request->post_body;
        $post->save();
        
        $response=
        [
            'post_id'       => $post->id,
            'created_at'    => $post->created_at->setTimezone('+06:00')->format('h:i a, M d, Y'),
            'people_id'     => $user->id,
            'people_name'   => $user->people->name,
        ];


        return response()->json($response);
    }

    public function removeCirclePost(Request $request)
    {
        Post::destroy($request->post_id);
        return response()->json();
    }

    public function insertCircleComment(Request $request)
    {
        $user=Auth::user();

        $comment=new Comment;
        $comment->people_id=$user->id;
        $comment->comment_body=$request->comment_body;
        $comment->post_id=$request->post_id;

        $comment->save();

        $response=
        [
            'comment_id'    => $comment->id,
            'people_id'     => $comment->people_id,
            'people_name'   => $comment->people->name,
        ];

        return response()->json($response);

    }

    public function removeCircleComment(Request $request)
    {
        Comment::destroy($request->comment_id);
        return response()->json();
    }

    public function insertCircleNotice(Request $request)
    {
        $user=Auth::user();

        $notice=new Notice; 
        $notice->circle_id=$request->circle_id;
        $notice->subject=$request->topic;
        $notice->details=$request->description;
        $notice->save();

        $this->insertNotification($user,$request->circle_id,'A notice was added in ');

        $response=
        [
            'created_at'    => $notice->created_at->setTimezone('+06:00')->format('h:i a, M d, Y'),
            'notice_id'     => $notice->id,
        ];

        return response()->json($response);

    }

    public function insertCircleClassTest(Request $request)
    {
        $user=Auth::user();

        $classtest=new Classtest;
        $classtest->circle_id=$request->circle_id;
        $classtest->syllabus=$request->syllabus;
        $classtest->schedule=Carbon::parse($request->schedule);
        $classtest->save();

        $this->insertNotification($user,$request->circle_id,'New classtest added to ');

        $response=
        [
            'schedule' => $classtest->schedule->format('h:i a, M d'),
            'classtest_id' => $classtest->id,
            'count' => $classtest->circle->classtests->count()
        ];

        return response()->json($response);
    }

    public function insertCircleAssignment(Request $request)
    {
        $user=Auth::user();

        $assignment=new Assignment;
        $assignment->circle_id=$request->circle_id;
        $assignment->details=$request->description;
        $assignment->deadline=Carbon::parse($request->deadline);
        $assignment->save();

        $this->insertNotification($user,$request->circle_id,'New assignment in ');

        $response=
        [
            'deadline' => $assignment->deadline->format('h:i a, M d'),
            'assignment_id' => $assignment->id,
            'count' => $assignment->circle->assignments->count()
        ];

        return response()->json($response);
    }

    public function removeCircleContent(Request $request)
    {
        if($request->type=='classtest') Classtest::destroy($request->id);
        else if($request->type=='file') File::destroy($request->id);
        else if($request->type=='notice') Notice::destroy($request->id);
        else if($request->type=='assignment') Assignment::destroy($request->id);

        return response()->json();
    }

    public function paginate(Request $request)
    {
        if($request->type=='files')
        {
            $circle=Circle::find($request->circle_id);
            $fileList=$circle->files()->where('is_special',0)->orderBy('created_at','desc')->skip($request->target)->take(5)->get();

            $response=['', ''];

            if($request->target==0) $response[0]='first';
            if($request->target+5>=$circle->files->where('is_special',0)->count()) $response[1]='last';

            foreach($fileList as $file)
            {
                array_push($response, 
                [
                    'file_id'       => $file->id,
                    'name'          => $file->name,
                    'size'          => $file->size,
                    'comment'       => $file->comment,
                ]);
            }

            return $response;
        }
        else if($request->type=='notice')
        {
            $circle=Circle::find($request->circle_id);
            $noticeList=$circle->notices()->orderBy('created_at','desc')->skip($request->target)->take(5)->get();

            $response=['', ''];

            if($request->target==0) $response[0]='first';
            if($request->target+5>=$circle->notices->count()) $response[1]='last';

            foreach($noticeList as $notice)
            {
                array_push($response, 
                [
                    'notice_id'     => $notice->id,
                    'topic'         => $notice->subject,
                    'description'   => $notice->details,
                    'created_at'    => $notice->created_at->setTimezone('+06:00')->format('h:i a, M d, Y'),
                ]);
            }

            return $response;
        }
    }

    function insertNotification($user,$circle_id,$msg)
    {
        $circle=Circle::find($circle_id);
        $enrollmentList=$circle->enrollments;

        foreach($enrollmentList as $enrollment)
        {
            if($enrollment->people_id==$user->id) continue;

            $notification=new Notification;
            $notification->msg=$msg."<a href='/circle/".$circle->id."'>".$circle->course->code."</a>";
            $notification->people_id=$enrollment->people_id;
            $notification->save();
        }
    }

    public function uploadSubmission(Request $request)
    {
        $user=Auth::user();
        $circle=Circle::find($request->circle_id);

        $f=$request->file('file');

        if($request->file_id!=0) $file=File::find($request->file_id);
        else $file=new File;

        $file->circle_id=$request->circle_id;
        $file->name=$f->getClientOriginalName();
        $file->size=$f->getSize();
        $file->mime=$f->getMimeType();
        $file->data=$f->openFile()->fread($f->getSize());
        $file->is_special=1;
        $file->save();

        if($request->file_id==0)
        {
            $submission=new Submission;
            $submission->file_id=$file->id;
            $submission->assignment_id=$request->assignment_id;
            $submission->student_id=$user->id;
            $submission->save();
        }

        return redirect()->back()
            ->with('status','success')
            ->with('msg','Submission successful.');
    }

    public function showSubmissions($id)
    {
        $user=Auth::user();
        $assignment=Assignment::find($id);

        $circle=$assignment->circle;

        $metainfo=Metainfo::all();
        $circleList=[];

        $current_session=$metainfo->where('key','Session')->first()->value;
        $enrollmentList=$user->people->enrollments;
        foreach($enrollmentList as $enrollment)
        {
            if($enrollment->circle->session==$current_session)
                array_push($circleList,$enrollment->circle);
        }

        $notificationCount=$user->people->notifications->where('is_seen',0)->count();
        $newMsgCount=Message::where('receiver_id',$user->id)->where('is_seen',0)->count();

        return view('BluPi-View.submissions',compact('user','circle','circleList','assignment','notificationCount','newMsgCount'));

    }
}