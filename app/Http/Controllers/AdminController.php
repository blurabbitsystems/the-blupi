<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\People;
use App\Models\Faculty;
use App\Models\Student;
use App\Models\Circle;
use App\Models\Department;
use App\Models\Metainfo;
use App\Models\Enrollment;
use App\Models\Course;
use Hash;
use Auth;
use DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['showLogin','authenticate']]);
        $this->middleware('admin', ['except' => ['showLogin','authenticate']]);
    }

    public function showLogin()
    {
        if(!Auth::guest())
    	{
    		return redirect('/admin/home');
    	}

    	$user=User::where('username','root')->first();

    	if(!$user)
    	{
            DB::statement('ALTER TABLE `files` CHANGE COLUMN `data` `data` MEDIUMBLOB NOT NULL');
            
    		$people=new People;
	    	$people->name='Root';
	    	$people->is_admin=1;
	    	$people->save();

	    	$user=new User;
            $user->id=$people->id;
	    	$user->username='root';
	    	$user->password=Hash::make('root');
	    	$user->save();

            $meta=new Metainfo;
            $meta->key='Institution-name';
            $meta->value='University of XYZ';
            $meta->save();

            $meta=new Metainfo;
            $meta->key='Session';
            $meta->value='2012-13';
            $meta->save();
    	}

        $metainfo=Metainfo::all();

    	return view('BluPi-View.adminlogin',compact('metainfo'));
    }

    public function authenticate(Request $request)
    {
        $user=User::where('username',$request->username)->first();

        if($user && !$user->people->is_admin)
            return redirect()->back()
                ->with('status','danger')
                ->with('msg',$request->username.' is not authorized to access.');

        $credentials=
        [
            'username' => $request->username,
            'password' => $request->password,
        ];

    	if(Auth::attempt($credentials,false)) return redirect('/admin/home');
			
		return redirect()->back()->withInput()
            ->with('status','danger')
            ->with('msg','Invalid credentials!');
    }

    public function showHome()
    {
        $user=Auth::user();
        $metainfo=Metainfo::all();

        if($user->username=='root')
    	{
            $adminList=People::where('is_admin',1)->get();
            $departmentList=Department::all();

            return view('BluPi-View.roothome',
                compact('user','adminList','departmentList','metainfo'));
        }
        else
        {
            if($user->people->department_id!=NULL)
            {
                $peopleList=People::where('is_admin',0)
                    ->where('department_id',$user->people->department_id)
                    ->get();
            }
            else $peopleList=NULL;

            if($user->people->department_id!=NULL)
            {
                $courseList=$user->people->department->courses->sortBy('code');
            }

            return view('BluPi-View.adminhome',
                compact('user','peopleList','courseList','metainfo'));
        }
    }

    public function setInstitutionName(Request $request)
    {
        $meta=Metainfo::where('key','Institution-name')->get()->first();
        $meta->value=$request->name;
        $meta->save();

        return redirect('/admin/home')
            ->with('status','success')
            ->with('msg','Institution has been renamed.');
    }

    public function setSession(Request $request)
    {
        $meta=Metainfo::where('key','Session')->get()->first();
        $meta->value=$request->session;
        $meta->save();

        $msg="Session changed";

        if($request->upgrade_students==true)
        {
            $studentList=Student::all();

            foreach($studentList as $student)
            {
                if($student->term==1) $student->term=2;
                else if($student->level==4)
                {
                    $student->level=NULL;
                    $student->term=NULL;
                }
                else if($student->level)
                {
                    $student->level++;
                    $student->term=1;
                }

                $student->save();
            }

            $msg.=' and student database upgraded';
        }

        return redirect('/admin/home')
            ->with('status','success')
            ->with('msg',$msg.'.');
    }

    public function insertAdmin(Request $request)
    {
        if(User::all()->contains('username',$request->username))
            return redirect()->back()
                    ->with('status','danger')
                    ->with('msg','Username \''.$request->username.'\' already exists!');

        $people=new People;
        $people->name=$request->name;
        $people->is_admin=1;
        $people->save();

        $user=new User;
        $user->id=$people->id;
        $user->username=$request->username;
        $user->password=Hash::make($request->password);
        $user->save();

        return redirect('/admin/home')
            ->with('status','success')
            ->with('msg',$people->name.' is added.');
    }

    public function insertDepartment(Request $request)
    {
        if(Department::all()->contains('codename',$request->codename))
            return redirect()->back()
                    ->with('status','danger')
                    ->with('msg',$request->codename.' department already exists!');

        $dept=new Department;
        $dept->fullname=$request->fullname;
        $dept->codename=$request->codename;
        $dept->save();

        return redirect('/admin/home')
            ->with('status','success')
            ->with('msg',$dept->fullname.' department is added successfully.');
    }

    public function removeDepartment(Request $request)
    {
        Department::destroy($request->department_id);
        return response()->json(['response'=>'Department deleted.']);
    }

    public function setDepartment(Request $request)
    {
        $people=People::find($request->people_id);
        $department=Department::where('codename',$request->codename)->first();

        if(!$department) $people->department_id=NULL;
        else $people->department_id=$department->id;
        
        $people->save();

        return redirect('/admin/home');
    }

    public function insertPeople(Request $request)
    {
        if(User::all()->contains('username',$request->username))
            return redirect()->back()
                ->with('status','danger')
                ->with('msg','Username \''.$request->username.'\' already exists!');

        $people=new People;
        $people->name=$request->name;
        $people->department_id=Auth::user()->people->department_id;
        $people->is_faculty=$request->is_faculty?1:0;
        $people->save();

        $user=new User;
        $user->id=$people->id;
        $user->username=$request->username;
        $user->password=Hash::make($request->password);
        $user->save();

        if($request->is_faculty)
        {
            $faculty=new Faculty;
            $faculty->id=$people->id;
            $faculty->save();
        }
        else
        {
            $student=new Student;
            $student->id=$people->id;
            $student->student_no=$request->username;
            $student->level=(int)(($request->levelterm+1)/2);
            $student->term=($request->levelterm-1)%2+1;
            $student->save();
        }

        return redirect('/admin/home')
            ->with('status','success')
            ->with('msg',$people->name.' is added.');
    }

    public function searchPeople(Request $request)
    {
        $metainfo=Metainfo::all();
        $user=Auth::user();
        $is_faculty=$request->is_faculty;

        if($is_faculty==0)
        {
            $studentList=Student::all()->filter
            (
                function($student)
                {
                    return $student->people->department_id==Auth::user()->people->department_id;
                }
            );

            if($request->levelterm!=0)
            {
                $level=(int)(($request->levelterm+1)/2);
                $term=($request->levelterm-1)%2+1;
                $studentList=$studentList->where('term',$term)->where('level',$level);
            }

            $studentList=$studentList->sortBy('term');
        }
        else
        {
            $facultyList=Faculty::all()->filter
            (
                function($faculty)
                {
                    return $faculty->people->department_id==Auth::user()->people->department_id;
                }
            );
        }

        return view('BluPi-View.adminsearch_people',
            compact('user','studentList','facultyList','is_faculty','metainfo'));
    }
    
    public function insertCourse(Request $request)
    {
        if(Course::all()->contains('code',$request->code))
            return redirect()->back()
                ->with('status','danger')
                ->with('msg','Course \''.$request->code.'\' already exists!');

        $user=Auth::user();

        $course=new Course;
        $course->name=$request->name;
        $course->code=$request->code;
        $course->department_id=$user->people->department_id;
        $course->save();

        return redirect('/admin/home')
            ->with('status','success')
            ->with('msg','Course \''.$request->code.'\' created successfully.');
    }

    public function createEnrollments(Request $request)
    {
        if(!$request->course_id_list)
            return redirect()->back()
                ->with('status','danger')
                ->with('msg','No course were selected!');

        $metainfo=Metainfo::all();
        $studentList=Student::all()->filter
        (
            function($student)
            {
                return $student->people->department_id==Auth::user()->people->department_id;
            }
        );

        $level=(int)(($request->levelterm+1)/2);
        $term=($request->levelterm-1)%2+1;
        $studentList=$studentList->where('term',$term)->where('level',$level);

        if($studentList->count()==0)
            return redirect()->back()
                ->with('status','warning')
                ->with('msg','No circle has been created because no students were in the selection.');

        $count=0;
        foreach($request->course_id_list as $course_id)
        {
            $circle=new Circle;
            $circle->session=$metainfo->where('key','Session')->first()->value;
            $circle->course_id=$course_id;
            $circle->department_id=Auth::user()->people->department_id;
            $circle->save();
            $count++;

            foreach($studentList as $student)
            {
                $enrollment=new Enrollment;
                $enrollment->circle_id=$circle->id;
                $enrollment->people_id=$student->id;
                $enrollment->is_admin=0;
                $enrollment->save();
            }
        }

        return redirect()->back()
            ->with('status','info')
            ->with('msg',$count.' circle(s) have been created. You must assign at least one course teacher to each enrollment to activate the corresponding circle.');
    }

    public function searchCircle(Request $request)
    {
        $metainfo=Metainfo::all();
        $user=Auth::user();
        $circleList=$user->people->department->circles->where('session',$request->session);

        if($request->course_id!=0)
            $circleList=$circleList->where('course_id',(int)$request->course_id);

        return view('BluPi-View.adminsearch_circle',
            compact('user','circleList','metainfo'));
    }

    public function showCircle($id)
    {
        $user=Auth::user();
        $circle=Circle::find($id);
        $metainfo=Metainfo::all();

        return view('BluPi-View.enrollment',
            compact('user','circle','metainfo'));
    }

    public function removeEnrollment(Request $request)
    {
        Enrollment::destroy($request->enrollment_id);
        return response()->json(['response'=>'Enrollment deleted.']);
    }

    public function removeCircle(Request $request)
    {
        Circle::destroy($request->circle_id);
        return response()->json(['response'=>'Circle deleted.']);
    }

    public function enrollIndividual(Request $request)
    {
        $user=User::where('username',$request->username)->first();

        if(!$user || $user->people->department_id!=Auth::user()->people->department_id)
            return redirect()->back()
                ->with('status','danger')
                ->with('msg','Username \''.$request->username.'\' does not exist!');

        if($user->people->enrollments->contains('circle_id',$request->circle_id))
            return redirect()->back()
                ->with('status','info')
                ->with('msg',$user->people->name.' is already enrolled.');

        $enrollment=new Enrollment;
        $enrollment->people_id=$user->people->id;
        $enrollment->circle_id=$request->circle_id;
        $enrollment->is_admin=$user->people->is_faculty;
        $enrollment->save();

        return redirect()->back()
            ->with('status','success')
            ->with('msg',$user->people->name.' is enrolled successfully.');
    }

    public function removePeople(Request $request)
    {
        People::destroy($request->people_id);
        return response()->json(['response'=>'Account deleted.']);
    }

    public function configPeople($id=NULL)
    {
        $metainfo=Metainfo::all();
        $user=Auth::user();
        
        if($id==NULL) $people=$user->people;
        else $people=People::find($id);
        
        if(!$people || $user->people->is_admin==0 ||
          ($people->id!=$user->id && $user->username!='root' && $people->is_admin==1) ||
          ($user->username!='root' && $user->people->department_id!=$people->department_id))
            return redirect('/admin/home')
                ->with('status','warning')
                ->with('msg','You are intentionally trying to upset my housekeeper!');

        return view('BluPi-View.config',compact('user','people','metainfo'));
    }

    public function configEdit($id)
    {
        $user=Auth::user();
        $metainfo=Metainfo::all();
        $people=People::find($id);

        if(!$people || $user->people->is_admin==0 ||
          ($people->id!=$user->id && $user->username!='root' && $people->is_admin==1) ||
          ($user->username!='root' && $user->people->department_id!=$people->department_id))
            return redirect('/admin/home')
                ->with('status','warning')
                ->with('msg','You are intentionally trying to upset my housekeeper!');

        return view('BluPi-View.adminedit',compact('user','people','metainfo'));
    }

    public function saveConfig(Request $request,$id)
    {
        if($request->password!=$request->password_retype)
            return redirect()->back()
                ->with('status','danger')
                ->with('msg','Password mismatch!');

        $user=Auth::user();
        $people=People::find($id);
        $people->name=$request->fullname;
        $people->save();

        if($request->password)
        {
            $people->user->password=Hash::make($request->password);
            $people->user->save();
        }

        if($people->is_admin==0)
        {
            if($people->is_faculty==0)
            {
                if($request->levelterm)
                {
                    $people->student->level=(int)(($request->levelterm+1)/2);
                    $people->student->term=($request->levelterm-1)%2+1;
                }
                else
                {
                    $people->student->level=NULL;
                    $people->student->term=NULL;
                }

                $people->student->major=$request->major;
                $people->student->minor=$request->minor;

                $people->student->save();
            }
            else
            {
                if($request->rank) $people->faculty->rank=$request->rank;
                if($request->research_area) $people->faculty->research_area=$request->research_area;

                $people->faculty->save();
            }
        }       

        return redirect('/admin/config/'.$id)
            ->with('status','success')
            ->with('msg','Changes are saved.');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/admin/login');
    }
}
