<?php

use App\Models\People;
use App\Models\File;
use App\Models\GeneralNotice;
use Illuminate\Http\Request;

Route::group(['middleware' => ['web']], function ()
{
	// Admin
	Route::get('/admin', 				'AdminController@showLogin');
	Route::get('/admin/login', 			'AdminController@showLogin');
	Route::get('/admin/home', 			'AdminController@showHome');
	Route::get('/admin/logout', 		'AdminController@logout');
	Route::get('/admin/config', 		'AdminController@configPeople');
	Route::get('/admin/config/{id}',	'AdminController@configPeople');
	Route::get('/admin/editconfig/{id}','AdminController@configEdit');
	Route::get('/admin/people', 		'AdminController@searchPeople');
	Route::get('/admin/circles', 		'AdminController@searchCircle');
	Route::get('/admin/circle/{id}', 	'AdminController@showCircle');
	Route::get('/admin/logout', 		'AdminController@logout');

	Route::post('admin_authentication', 'AdminController@authenticate');
	Route::post('set_institution_name', 'AdminController@setInstitutionName');
	Route::post('set_session', 			'AdminController@setSession');
	Route::post('insert_admin', 		'AdminController@insertAdmin');
	Route::post('insert_department',	'AdminController@insertDepartment');
	Route::post('remove_department', 	'AdminController@removeDepartment');
	Route::post('set_department', 		'AdminController@setDepartment');
	Route::post('insert_people', 		'AdminController@insertPeople');
	Route::post('remove_people', 		'AdminController@removePeople');
	Route::post('insert_course', 		'AdminController@insertCourse');
	Route::post('create_enrollments', 	'AdminController@createEnrollments');
	Route::post('remove_enrollment', 	'AdminController@removeEnrollment');
	Route::post('remove_circle',	 	'AdminController@removeCircle');
	Route::post('enroll_individual', 	'AdminController@enrollIndividual');
	Route::post('save_config/{id}', 	'AdminController@saveConfig');


	// Users
	Route::get('/', 					'UserController@showLogin');
	Route::get('/login', 				'UserController@showLogin');
	Route::get('/home', 				'UserController@showHome');
	Route::get('/inbox',				'UserController@showinbox');
	Route::get('/profile',				'UserController@showProfile');
	Route::get('/profile/{id}',			'UserController@showProfile');
	Route::get('/editprofile',			'UserController@editProfile');
	Route::get('/logout', 				'UserController@logout');
	Route::get('/search_people',		'UserController@searchPeople');
	Route::get('/circle/{id}',			'UserController@showCircle');
	Route::get('/post/{id}',			'UserController@showGeneralPost');
	Route::get('/circle/post/{id}',		'UserController@showCirclePost');
	Route::get('/searchresult',			'UserController@showSearchResult');
	Route::get('/circle/assignment/{id}/submissions',			'UserController@showSubmissions');

	Route::post('/login', 				'UserController@authenticate');
	Route::post('/insert_gen_post', 	'UserController@insertGeneralPost');
	Route::post('/remove_gen_post', 	'UserController@removeGeneralPost');
	Route::post('/insert_gen_comment',	'UserController@insertGeneralComment');
	Route::post('/remove_gen_comment',	'UserController@removeGeneralComment');
	Route::post('/insert_gen_notice',	'UserController@insertGeneralNotice');
	Route::post('/remove_gen_notice',	'UserController@removeGeneralNotice');
	Route::post('/fetch_notifications',	'UserController@fetchNotifications');
	Route::post('/save_profile',		'UserController@saveProfile');
	Route::post('/send_message',		'UserController@sendMessage');
	Route::post('/set_message_seen',	'UserController@setMessageSeen');
	Route::post('/insert_file',				'UserController@insertFile');
	Route::post('/insert_circle_notice',	'UserController@insertCircleNotice');
	Route::post('/insert_circle_classtest',	'UserController@insertCircleClassTest');
	Route::post('/insert_circle_assignment','UserController@insertCircleAssignment');
	Route::post('/remove_circle_content',	'UserController@removeCircleContent');
	Route::post('/insert_circle_post', 		'UserController@insertCirclePost');
	Route::post('/remove_circle_post', 		'UserController@removeCirclePost');
	Route::post('/insert_circle_comment',	'UserController@insertCircleComment');
	Route::post('/remove_circle_comment',	'UserController@removeCircleComment');
	Route::post('/paginate',				'UserController@paginate');	
	Route::post('/upload_submission',		'UserController@uploadSubmission');	


	// Misc
	Route::group(['middleware' => ['auth']], function ()
	{
		Route::get('/images/avatar_{id}.jpg', 	function($id)
		{
			$picture=People::find($id)->picture;

	        if($picture) return response($picture)->header('Content-Type', 'image/jpeg');
	        else return redirect('/images/default.jpg');
		});

		Route::get('/images/avatar.jpg', 		function()
		{
			$picture=Auth::user()->people->picture;

	        if($picture) return response($picture)->header('Content-Type', 'image/jpeg');
	        else return redirect('/images/default.jpg');
		});

		Route::get('/files/{id}/{fname}',		function($id,$fname)
		{
			$file=File::find($id);

	        return response($file->data)->header('Content-Type', $file->mime);
		});

		Route::post('/next_general_notice',		function(Request $request)
		{
			$user=Auth::user();

			if($user->people->is_faculty==1)
				$notice=GeneralNotice::orderBy('created_at','desc')->skip($request->page)->first();
			else
				$notice=GeneralNotice:: where('scope_level',$user->people->student->level)
	                ->where('scope_term',$user->people->student->term)
	                ->orWhere('scope_term',0)
	                ->orderby('created_at','desc')->skip($request->page)->first();

			$response=
			[
				'notice_id'		=> $notice->id,
				'subject'		=> $notice->subject,
				'body'			=> $notice->body,
				'created_at'	=> $notice->created_at->setTimezone('+06:00')->format('h:i a, M d, Y'),
				'is_faculty'	=> $user->people->is_faculty
			];

			return $response;
		});
	});
});
