<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	//protected $table='courses';

    public function department()
    {
    	return $this->belongsTo('App\Models\Department');
    }

    public function circles()
    {
    	return $this->hasMany('App\Models\Circle');
    }
}
