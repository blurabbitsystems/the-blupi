<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $table='peoples';

    public function posts()
    {
    	return $this->hasMany('App\Models\Post');
    }

    public function genposts()
    {
    	return $this->hasMany('App\Models\GeneralPost');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function gencomments()
    {
        return $this->hasMany('App\Models\GeneralComment');
    }

    public function enrollments()
    {
        return $this->hasMany('App\Models\Enrollment');
    }

    public function department()
    {
    	return $this->belongsTo('App\Models\Department');
    }

    public function student()
    {
        return $this->hasOne('App\Models\Student','id');
    }

    public function faculty()
    {
        return $this->hasOne('App\Models\Faculty','id');
    }

    public function user()
    {
        return $this->hasOne('App\User','id');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\Notification');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    public function gennotices()
    {
        return $this->hasMany('App\Models\GeneralNotice');
    }
}
