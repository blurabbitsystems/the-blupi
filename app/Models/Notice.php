<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
	//protected $table='notices';

    public function circle()
    {
    	return $this->belongsTo('App\Models\Circle');
    }
}
