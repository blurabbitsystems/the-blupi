<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
	//protected $table='students';

    public function people()
    {
    	return $this->belongsTo('App\Models\People','id');
    }

    public function submissions()
    {
    	return $this->hasMany('App\Models\Submission','id');
    }
}
