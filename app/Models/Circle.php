<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Circle extends Model
{
    //protected $table='circles';

    public function enrollments()
    {
    	return $this->hasMany('App\Models\Enrollment');
    }

    public function assignments()
    {
    	return $this->hasMany('App\Models\Assignment');
    }

    public function classtests()
    {
    	return $this->hasMany('App\Models\Classtest');
    }

    public function notices()
    {
    	return $this->hasMany('App\Models\Notice');
    }

    public function posts()
    {
    	return $this->hasMany('App\Models\Post');
    }

    public function files()
    {
        return $this->hasMany('App\Models\File');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }    
}
