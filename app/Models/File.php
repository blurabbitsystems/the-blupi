<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	//protected $table='files';

    public function circle()
    {
    	return $this->belongsTo('App\Models\Circle');
    }
}
