<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classtest extends Model
{
	//protected $table='classtests';
	protected $dates=['schedule'];
	
	public function circle()
	{
		return $this->belongsTo('App\Models\Circle');
	}
}
