<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function people()
    {
    	return $this->belongsTo('App\Models\People');
    }

    public function receiver()
    {
    	return $this->belongsTo('App\Models\People','receiver_id');
    }
}
