<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\GeneralPostComment;

class GeneralPost extends Model
{
    //protected $table='general_posts';

    public function comments()
    {
    	return $this->hasMany('App\Models\GeneralComment');
    }

    public function people()
    {
    	return $this->belongsTo('App\Models\People');
    }
}
