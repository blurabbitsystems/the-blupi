<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralComment extends Model
{
    //protected $table='general_comments';

    public function post()
    {
    	return $this->belongsTo('App\Models\GeneralPost');
    }

    public function people()
    {
    	return $this->belongsTo('App\Models\People');
    }
}
