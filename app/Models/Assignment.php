<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    //protected $table='assignments';
	protected $dates=['deadline'];

    public function circle()
    {
    	return $this->belongsTo('App\Models\Circle');
    }

    public function submissions()
    {
    	return $this->hasMany('App\Models\Submission');
    }
}
