<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
    //protected $table='enrollments';

    public function circle()
    {
    	return $this->belongsTo('App\Models\Circle');
    }

    public function people()
    {
    	return $this->belongsTo('App\Models\People');
    }
}
