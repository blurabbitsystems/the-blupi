<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //protected $table='departments';

    public function people()
    {
    	return $this->hasMany('App\Models\People');
    }

    public function circles()
    {
    	return $this->hasMany('App\Models\Circle');
    }

    public function courses()
    {
    	return $this->hasMany('App\Models\Course');
    }
}
