<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	//protected $table='posts';

    public function comments()
    {
    	return $this->hasMany('App\Models\Comment');
    }

    public function circle()
    {
    	return $this->belongsTo('App\Models\Circle');
    }

    public function people()
    {
    	return $this->belongsTo('App\Models\People');
    }
}
