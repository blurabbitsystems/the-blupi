<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
	//protected $table='submissions';

    public function file()
    {
    	return $this->belongsTo('App\Models\File');
    }

    public function assignment()
    {
    	return $this->belongsTo('App\Models\Assignment');
    }

    public function student()
    {
    	return $this->belongsTo('App\Models\Student');
    }
}
