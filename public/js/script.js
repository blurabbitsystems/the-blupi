function insertGeneralPost()
{
    var newPostNode=document.getElementById('blupi-new-post');
    var postNode=document.getElementById('blupi-post');

    var levelterm=document.getElementById('setAudience').getElementsByTagName('select')[0].value;
    var textarea=newPostNode.children[1], post_body=textarea.value;
    var checkbox=newPostNode.children[2].getElementsByTagName('input')[0], comment_disabled=checkbox.checked?1:0;

    $.ajax
    ({
        url: '/insert_gen_post',
        type: "post",
        data:
        {
            'post_body': post_body,
            'comment_disabled': comment_disabled,
            'levelterm': levelterm
        },
        success: function(data)
        {
            var post_id=data['post_id'];
            var created_at=data['created_at'];
            var people_id=data['people_id'];
            var people_name=data['people_name'];

            var comment_form='';

            if(comment_disabled==0)
            {
                comment_form=
                    '<div>'+
                        '<textarea name="comment" class="form-control blupi-comment-field" placeholder="Write a comment..." maxlength="255"></textarea>'+
                        '<button class="btn btn-default blupi-comment-button"'+
                                'onclick="insertGeneralComment(\''+ post_id +'\')">' +
                            'Submit'+
                        '</button>'+
                    '</div>';
            }   

            var htmlcontent=
                '<div class="row blupi-post-header">'+

                    '<div class="col-md-1">'+
                        '<img src="images/avatar.jpg" class="img-circle" width="55" height="55">'+
                    '</div>'+

                    '<div class="col-md-11" style="padding-top: 5px">'+
                        '<div class="blupi-post-name">'+
                            '<a class="blupi-a" href="/profile/'+people_id+'">'+people_name+'</a>'+
                        '</div>'+

                        '<div class="blupi-post-date">'+
                            created_at +
                        '</div>'+
                    '</div>'+
                '</div>'+

                '<hr>'+

                '<div class="blupi-post-body">'+
                    post_body +
                    '<br>'+
                    '<small style="cursor: pointer">'+
                        '[<span class="text-danger" onclick="removeGeneralPost('+post_id+')">remove</span>]'+
                    '</small>'+
                '</div>'+

                '<hr>'+

                '<div id="blupi-post-'+ post_id +'-comment">'+
                    comment_form+
                '</div>';

            var elem=document.createElement('div');
            elem.className='blupi-post';
            elem.innerHTML=htmlcontent;
            elem.id='blupi-post-'+post_id;
            postNode.insertBefore(elem,postNode.firstElementChild);

            textarea.value="";
            checkbox.checked=false;
        }
    });    
}

function removeGeneralPost(post_id)
{
    
    $.ajax
    ({
        url: '/remove_gen_post',
        type: "post",
        data: {'post_id': post_id},
        success: function(data)
        {
            document.getElementById('blupi-post-'+post_id).remove();
        }
    });
}

function insertGeneralComment(post_id)
{
    var commentNode=document.getElementById('blupi-post-'+post_id+'-comment');
    var textarea=commentNode.getElementsByTagName('textarea')[0];
    var comment_body=textarea.value;

    $.ajax
    ({
        url: '/insert_gen_comment',
        type: "post",
        data:
        {
            'comment_body': comment_body,
            'post_id': post_id
        },
        success: function(data)
        {
            var post_id=data['post_id'];
            var comment_id=data['comment_id'];
            var people_id=data['people_id'];
            var people_name=data['people_name'];

            var htmlcontent=
                '<div class="blupi-comment-name">'+
                    '<a class="blupi-a" href="/profile/'+people_id+'">' + people_name + '</a> says:'+
                '</div>'+

                '<div class="blupi-comment-body">'+
                    comment_body+
                    '<br>'+
                    '<small style="cursor: pointer">'+
                        '[<span class="text-danger" onclick="removeGeneralComment('+post_id+','+comment_id+')">remove</span>]'+
                    '</small>'+
                '</div>'+

                '<hr>';

            var elem=document.createElement('div');
            elem.id='blupi-post-'+post_id+'-comment-'+comment_id;
            elem.innerHTML=htmlcontent;
            commentNode.insertBefore(elem,commentNode.lastElementChild);

            textarea.value='';
        }
    }); 
}

function removeGeneralComment(post_id,comment_id)
{
    
    $.ajax
    ({
        url: '/remove_gen_comment',
        type: "post",
        data: {'comment_id': comment_id},
        success: function(data)
        {
            document.getElementById('blupi-post-'+post_id+'-comment-'+comment_id).remove();
        }
    });
}

function insertGeneralNotice()
{
    var panel=document.getElementById('general-notice-body');
    var topic_field=document.getElementById('general-notice-topic'), topic=topic_field.value;
    var description_field=document.getElementById('general-notice-description'), description=description_field.value;

    var levelterm=document.getElementById('setAudience').getElementsByTagName('select')[0].value;
    
    $.ajax
    ({
        url: '/insert_gen_notice',
        type: "post",
        data:
        {
            'topic': topic,
            'description': description,
            'levelterm': levelterm
        },
        success: function(data) 
        {
            var created_at=data['created_at'];
            var notice_id=data['notice_id'];

            var htmlcontent=
                '<small onclick="removeGeneralNotice(this.parentNode,\''+notice_id+'\')"'+
                'style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>'+
                '<center><b>'+topic+'</b></center>'+
                '<p>'+description+'</p>'+
                '<small>'+created_at+'</small>'+
                '<hr>';
 
            var elem=document.createElement("div");
            elem.innerHTML=htmlcontent;

            panel.insertBefore(elem,panel.firstElementChild);
        }

    });     
}

function removeGeneralNotice(node,id)
{
    $.ajax
    ({
        url: '/remove_gen_notice',
        type: "post",
        data: {'notice_id': id},
        success: function(data)
        {
            node.remove();
        }
    });
}

function showSidebarForm(id,circle_id=0)
{
    if(document.getElementById(id+'-form'))
    {
        document.getElementById(id+'-form').remove();
        return;
    }

    var today=new Date().toJSON().slice(0,10);

    if(id=='general-notice')
    {
        var htmlcontent=
            '<strong>Topic:</strong><br>'+
            '<input type="text" class="form-control" name="topic" id="general-notice-topic" maxlength="30">'+
            '<strong>Description:</strong><br>'+
            '<textarea name="description" id="general-notice-description" class="form-control" style="margin-bottom: 5px" maxlength="150"></textarea>'+

            '<div class="blupi-sidebar-item" align="right">'+
                '<button class="btn btn-default" style="background: #2d672d" data-toggle="modal" data-target="#setAudience">'+
                '<img src="/images/audience-icon.png" width="30" height="30">'+
                '</button>'+

                '<button onclick="insertGeneralNotice(); hideSidebarForm(\''+id+'\')" class="btn btn-primary">' +
                '<img src="/images/ok-icon.png" width="30" height="30">' +
                '</button>'+

                '<button onclick="hideSidebarForm(\''+id+'\')" class="btn btn-danger">' +
                '<img src="/images/cancel-icon.png" width="30" height="30">' +
                '</button>'+
            '</div>';


    }
    else if(id=='file-explorer')
    {
        var htmlcontent=
        '<form method="post" onsubmit="insertFile(this,\''+circle_id+'\'); return false;" enctype="multipart/form-data">'+
            '<strong>Select File:</strong><br>'+
            '<input type="file" name="file">'+

            '<strong>Comment:</strong><br>'+
            '<input name="comment" class="form-control" style="margin-bottom: 5px" autocomplete="off" maxlength="80">'+

            '<div class="blupi-sidebar-item" align="right">'+
                '<button type="submit" class="btn btn-primary">' +
                    '<img src="/images/ok-icon.png" width="30" height="30">' +
                '</button>'+

                '<button type="button" onclick="hideSidebarForm(\''+id+'\')" class="btn btn-danger">' +
                    '<img src="/images/cancel-icon.png" width="30" height="30">' +
                '</button>'+
            '</div>'+
        '</form>';
    }
    else if(id=='class-test')
    {
        var htmlcontent=
            '<center><b>Add Class Test</b></center>'+

            '<b>Syllabus:</b><br>'+
            '<textarea class="form-control" style="margin-bottom: 5px" maxlength="150"></textarea>'+

            '<b>Schedule:</b><br>' +
            '<input type="datetime-local" value="'+today+'T23:59" style="border-radius: 5px">'+

            '<div class="blupi-sidebar-item" align="right" style="margin-top: 5px">'+
                '<strong class="text-danger"></strong>'+

                '<button onclick="insertClassTest(this.parentNode.parentNode,\''+circle_id+'\');" class="btn btn-primary">' +
                    '<img src="/images/ok-icon.png" width="30" height="30">' +
                '</button>'+

                '<button onclick="hideSidebarForm(\''+id+'\')" class="btn btn-danger">' +
                    '<img src="/images/cancel-icon.png" width="30" height="30">' +
                '</button>'+
            '</div>';
    }
    else if(id=='assignment')
    {
        var htmlcontent=
            '<center><b>Add Assignment</b></center>'+

            '<b>Description:</b><br>'+
            '<textarea class="form-control" style="margin-bottom: 5px" maxlength="255"></textarea>'+

            '<b>Deadline:</b><br>' +
            '<input type="datetime-local" value="'+today+'T23:59" style="border-radius: 5px">'+

            '<div class="blupi-sidebar-item" align="right">'+
                '<strong class="text-danger"></strong>'+

                '<button onclick="insertAssignment(this.parentNode.parentNode,\''+circle_id+'\');" class="btn btn-primary">' +
                    '<img src="/images/ok-icon.png" width="30" height="30">' +
                '</button>'+

                '<button onclick="hideSidebarForm(\''+id+'\')" class="btn btn-danger">' +
                    '<img src="/images/cancel-icon.png" width="30" height="30">' +
                '</button>'+
            '</div>';
    }
    else if(id=='notice-board')
    {
        var htmlcontent=
            '<strong>Topic:</strong><br>'+
            '<input type="text" class="form-control" name="topic" maxlength="30">'+
            '<strong>Description:</strong><br>'+
            '<textarea name="description" id="" class="form-control" style="margin-bottom: 5px" maxlength="150"></textarea>'+

            '<div class="blupi-sidebar-item" align="right">'+
                '<strong class="text-danger"></strong>'+

                '<button onclick="insertCircleNotice(this.parentNode.parentNode,'+circle_id+');" class="btn btn-primary">' +
                    '<img src="/images/ok-icon.png" width="30" height="30">' +
                '</button>'+
                '<button onclick="hideSidebarForm(\''+id+'\')" class="btn btn-danger">' +
                    '<img src="/images/cancel-icon.png" width="30" height="30">' +
                '</button>'+
            '</div>';

    }


    var elem=document.createElement("div");
    elem.className="blupi-sidebar-form";
    elem.id=id+'-form';
    elem.innerHTML=htmlcontent;

    document.getElementById(id).appendChild(elem);
}

function hideSidebarForm(id)
{
    var elem=document.getElementById(id+'-form');
    document.getElementById(id).removeChild(elem);
}


function insertCircleNotice(node,circle_id) // eita korbi
{
    var panel=document.getElementById('notice-panel');

    var topic=node.getElementsByTagName('input')[0];
    var description=node.getElementsByTagName('textarea')[0];

    if(topic.value=="")
    {
        node.getElementsByTagName('strong')[2].innerHTML="Topic is empty!";
        return;
    }

    if(description.value=="")
    {
        node.getElementsByTagName('strong')[2].innerHTML="Description is empty!";
        return;
    }

    $.ajax
    ({
        url: '/insert_circle_notice',
        type: "post",
        data:
        {
            'topic': topic.value,
            'description': description.value,
            'circle_id' : circle_id,
        },
        success: function(data) 
        {
            var created_at=data['created_at'];
            var notice_id=data['notice_id'];

            var htmlcontent=
                '<small onclick="removeCircleContent(this.parentNode,\''+notice_id+'\',\'notice\')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>'+
                '<center><b>'+topic.value+'</b></center>'+
                '<pre style="white-space: pre-wrap; font-family: blupi-font; font-size: 100%; color: #000;">'+description.value+'</pre>'+
                '<small>'+created_at+'</small>'+
                '<hr>';

            var elem=document.createElement("div");
            elem.innerHTML=htmlcontent;

            panel.insertBefore(elem,panel.firstElementChild);
            node.remove();
        }

    });
}

function insertClassTest(node,circle_id)    // eita korbi.
{
    var panel=document.getElementById('ct-panel');

    var syllabus=node.getElementsByTagName('textarea')[0].value;
    var schedule=node.getElementsByTagName('input')[0].value;

    if(syllabus=="")
    {
        node.getElementsByTagName('strong')[0].innerHTML="Syllabus is empty!";
        return;
    }

    $.ajax
    ({
        url: '/insert_circle_classtest',
        type: "post",
        data:
        {
            'syllabus' : syllabus,
            'schedule' : schedule,
            'circle_id' : circle_id,
        },
        success: function(data) 
        {
            var classtest_id=data['classtest_id'];
            var schedule=data['schedule'];
            var count=data['count'];

            var htmlcontent=
                '<small onclick="removeCircleContent(this.parentNode,\''+classtest_id+'\',\'classtest\')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>'+
                '<center><b>Class Test '+count+'</b></center>'+
                '<b>Syllabus:</b> <pre style="white-space: pre-wrap; font-family: blupi-font; font-size: 100%; color: #000;">'+syllabus+'</pre>'+
                '<b>Schedule:</b> '+schedule+
                '<hr>';

            var elem=document.createElement("div");
            elem.innerHTML=htmlcontent;

            panel.appendChild(elem);
            node.remove();
        }

    }); 
}

function insertAssignment(node,circle_id)   // eita korbi.
{
    var panel=document.getElementById('assignment-panel');

    var description=node.getElementsByTagName('textarea')[0].value;
    var deadline=node.getElementsByTagName('input')[0].value;
    

    $.ajax
    ({
        url: '/insert_circle_assignment',
        type: "post",
        data:
        {
            'description' : description,
            'deadline' : deadline,
            'circle_id' : circle_id,
        },
        success: function(data) 
        {
            var assignment_id=data['assignment_id'];
            var deadline=data['deadline'];
            var count=data['count'];

            var htmlcontent=
                '<small onclick="removeCircleContent(this.parentNode,\''+assignment_id+'\',\'assignment\')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>'+
                '<center><b>Assignment </b></center>'+
                '<b>Description:</b> <pre style="white-space: pre-wrap; font-family: blupi-font; font-size: 100%; color: #000;">'+description+'</pre>'+
                '<b>Deadline:</b> '+deadline+
                '<hr>';

            var elem=document.createElement("div");
            elem.innerHTML=htmlcontent;

            panel.appendChild(elem);
            node.remove();
        }
    }); 


}

function insertFile(node,circle_id)
{
    var panel=document.getElementById('file-panel');

    var filesize=node.getElementsByTagName('input')[0].files[0].size;
    var comment=node.getElementsByTagName('input')[1].value;

    var fd=new FormData(node);
    fd.append('circle_id',circle_id);

    var progressbar=
    '<strong>Upload progress:</strong>'+
    '<div class="progress">'+
        '<div id="uploadprogress" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>'+
    '</div>'+
    '<hr>';

    var elem=document.createElement("div");
    elem.innerHTML=progressbar;

    panel.insertBefore(elem,panel.firstElementChild);

    $.ajax
    ({
        url: '/insert_file',

        xhr: function()
        {
            var xhr=new XMLHttpRequest();

            xhr.upload.addEventListener("progress", function(event)
            {
                var loaded=(event.loaded/filesize).toFixed(2)*100;
                document.getElementById("uploadprogress").style.width=loaded+'%';

            }, false);

            return xhr;
        },

        type: 'post',
        processData: false,
        contentType: false,

        data: fd,

        success: function(data)
        {
            var id=data['id'], name=data['name'], size=data['size'];

            var htmlcontent=
                '<small onclick="removeCircleContent(this.parentNode,\''+id+'\',\'file\')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>'+
                '<b>File: </b><a style="color: #014c8c" href="/files/'+id+'/'+name+'">'+name+'</a><br>'+
                '<b>Size: </b>'+formatBytes(size)+'<br>'+
                '<b>Comment: </b>'+comment+
                '<hr>';

            elem.remove();
            elem=document.createElement("div");
            elem.innerHTML=htmlcontent;

            panel.insertBefore(elem,panel.firstElementChild);
            node.parentNode.remove();
        }
    });

            
}

function removeCircleContent(node,id,type)
{
    $.ajax
    ({
        url: '/remove_circle_content',
        type: "post",
        data:
        {
            'type': type,
            'id': id
        },
        success: function(data)
        {
            node.remove();
        }
    });
}

function insertCirclePost(circle_id,personName)
{
    //window.alert('Incomplete.');
    var newPostNode=document.getElementById('blupi-new-post');
    var postNode=document.getElementById('blupi-post');

    var textarea=newPostNode.children[1], post_body=textarea.value;
    
    $.ajax
    ({
        url: '/insert_circle_post',
        type: "post",
        data:
        {
            'post_body': post_body,
            'circle_id': circle_id,
        },
        success: function(data)
        {
            var post_id=data['post_id'];
            var created_at=data['created_at'];
            var people_id=data['people_id'];
            var people_name=data['people_name'];

            var htmlcontent=
                '<div class="row blupi-post-header">'+

                    '<div class="col-md-1">'+
                        '<img src="/images/avatar_'+people_id+'.jpg" class="img-circle" width="55" height="55">'+
                    '</div>'+

                    '<div class="col-md-11" style="padding-top: 5px">'+
                        '<div class="blupi-post-name">'+
                            '<a class="blupi-a" href="/profile/'+people_id+'">'+people_name+'</a>'+
                        '</div>'+

                        '<div class="blupi-post-date">'+
                            created_at +
                        '</div>'+
                    '</div>'+
                '</div>'+

                '<hr>'+

                '<div class="blupi-post-body">'+
                    post_body +
                    '<br>'+
                    '<small style="cursor: pointer">'+
                        '[<span class="text-danger" onclick="removeCirclePost('+post_id+')">remove</span>]'+
                    '</small>'+
                '</div>'+

                '<hr>'+

                '<div id="blupi-post-'+ post_id +'-comment">'+
                    '<div>'+
                        '<textarea name="comment" class="form-control blupi-comment-field" placeholder="Write a comment..."></textarea>'+
                        '<button class="btn btn-default blupi-comment-button"'+
                                'onclick="insertCircleComment(\''+ post_id +'\')">' +
                            'Submit'+
                        '</button>'+
                    '</div>'+
                '</div>';

            var elem=document.createElement('div');
            elem.className='blupi-post';
            elem.innerHTML=htmlcontent;
            elem.id='blupi-post-'+post_id;
            postNode.insertBefore(elem,postNode.firstElementChild);

            textarea.value="";
        }
    });  
}

function removeCirclePost(post_id)
{
    
    $.ajax
    ({
        url: '/remove_circle_post',
        type: "post",
        data: {'post_id': post_id},
        success: function(data)
        {
            document.getElementById('blupi-post-'+post_id).remove();
        }
    });
}

function insertCircleComment(post_id)
{
    
    var commentNode=document.getElementById('blupi-post-'+post_id+'-comment');
    var textarea=commentNode.getElementsByTagName('textarea')[0];
    var comment_body=textarea.value;

    $.ajax
    ({
        url: '/insert_circle_comment',
        type: "post",

        data:
        {
            'comment_body': comment_body,
            'post_id': post_id
        },
        success: function(data)
        {
            var post_id=data['post_id'];
            var comment_id=data['comment_id'];
            var people_id=data['people_id'];
            var people_name=data['people_name'];

            var htmlcontent=
                '<div class="blupi-comment-name">'+
                    '<a class="blupi-a" href="/profile/'+people_id+'">' + people_name + '</a> says:'+
                '</div>'+

                '<div class="blupi-comment-body">'+
                    comment_body+
                    '<br>'+
                    '<small style="cursor: pointer">'+
                        '[<span class="text-danger" onclick="removeCircleComment('+post_id+','+comment_id+')">remove</span>]'+
                    '</small>'+
                '</div>'+

                '<hr>';

            var elem=document.createElement('div');
            elem.id='blupi-post-'+post_id+'-comment-'+comment_id;
            elem.innerHTML=htmlcontent;
            commentNode.insertBefore(elem,commentNode.lastElementChild);

            textarea.value='';
        }
    }); 
}

function removeCircleComment(post_id,comment_id)
{
    
    $.ajax
    ({
        url: '/remove_circle_comment',
        type: "post",
        data: {'comment_id': comment_id},
        success: function(data)
        {
            document.getElementById('blupi-post-'+post_id+'-comment-'+comment_id).remove();
        }
    });
}

function deEnroll(node,enrollment_id)
{
    $.ajax
    ({
        url: '/remove_enrollment',
        type: "post",
        data: {'enrollment_id': enrollment_id},
        success: function(data)
        {
            var row=node.parentNode.parentNode;
            row.style.opacity=0.2;
            node.style.visibility='hidden';
        }
    });
}

function removeAdmin(node,people_id)    // done
{
    $.ajax
    ({
        url: '/remove_people',
        type: "post",
        data: {'people_id': people_id},
        success: function(data)
        {
            var row=node.parentNode.parentNode;
            row.style.opacity=0.2;
            node.style.visibility='hidden';
        }
    });
}

function removeCircle(node,circle_id)    // done
{
    $.ajax
    ({
        url: '/remove_circle',
        type: "post",
        data: {'circle_id': circle_id},
        success: function(data)
        {
            var row=node.parentNode.parentNode;
            row.style.opacity=0.2;
            node.style.visibility='hidden';
            row.children[2].style.visibility='hidden';
        }
    });
}

function removePeople(node,people_id)
{
    $.ajax
    ({
        url: '/remove_people',
        type: "post",
        data: {'people_id': people_id},
        success: function(data)
        {
            var row=node.parentNode.parentNode;
            row.style.opacity=0.2;
            node.style.visibility='hidden';
        }
    });
}

function removeDepartment(node,department_id)   // done
{
    $.ajax
    ({
        url: '/remove_department',
        type: "post",
        data: {'department_id': department_id},
        success: function(data)
        {
            var row=node.parentNode.parentNode;
            row.style.opacity=0.2;
            node.style.visibility='hidden';
        }
    });
}

function setDepartmentID(id)
{
    document.getElementById('setDepartment_Input').value=id;
}


function fetchNotifications()
{
    $panel=document.getElementById('notifications_panel');
    $panel.innerHTML='Loading...';

    $.ajax
    ({
        url: '/fetch_notifications',
        type: "post",
        data: {},
        success: function(data)
        {
            $htmlcontent='';

            for(var i=0;i<data['notifications'].length;i++)
                $htmlcontent+=(i+1)+'. '+data['notifications'][i]+'<br>';

            if($htmlcontent=='') $htmlcontent='No new notifications.';

            $panel.innerHTML=$htmlcontent;
            
            document.getElementById('notification_count').remove();
        }
    });
}

function setMessageSeen(message_id)
{
    $.ajax
    ({
        url: '/set_message_seen',
        type: "post",
        data: {'message_id': message_id}
    });
}

function searchPeople(name)
{
    var panel=document.getElementById('search_people_result');

    if(name.length<3)
    {
        panel.innerHTML="";
        return;
    }

    var xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            var data=JSON.parse(xmlhttp.responseText);

            var names='';
            for(var i=0;i<data.length;i++)
            {
                names+='<div class="blupi-search-result-item"'+
                            'onclick="selectSearchResult(this,\''+data[i]['id']+'\')">'+
                            data[i]['name']+
                        '</div>';
            }

            if(names=='') names='Person not found.';

            var htmlcontent='<div class="blupi-search-result">'+names+'</div>';
            panel.innerHTML=htmlcontent;
        }
    };

    xmlhttp.open("get", "/search_people?name="+name, true);
    xmlhttp.send();
}


function selectSearchResult(node,id)
{
    document.getElementById('receiver_name').value=node.innerHTML;
    document.getElementById('receiver_id').value=id;

    node.parentNode.remove();
}

function fetchGeneralNotice(node)
{
    var panel=document.getElementById('general-notice-body');

    $.ajax
    ({
        url: '/next_general_notice',
        type: 'post',
        data:
        {
            'page': node.getElementsByTagName('input')[0].value
        },
        success: function(data)
        {
            var notice_id=data['notice_id'];
            var subject=data['subject'];
            var body=data['body'];
            var created_at=data['created_at'];
            var is_faculty=data['is_faculty'];

            var htmlcontent=
                '<center><b>'+subject+'</b></center>'+
                '<p>'+body+'</p>'+
                '<small>'+created_at+'</small>'+
                '<hr>';

            if(is_faculty==1)
                htmlcontent='<small onclick="removeGeneralNotice(this.parentNode,\''+notice_id+'\')"'+
                'style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>'+htmlcontent;
 
            var elem=document.createElement("div");
            elem.innerHTML=htmlcontent;

            panel.insertBefore(elem,panel.lastElementChild);

            node.getElementsByTagName('input')[0].value++;
        }
    });
}

function formatBytes(bytes, decimals=2)
{
    if(bytes==0) return "0 byte";
    var k = 1024;
    var sizes = ["byte", "K", "M"];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(decimals)) + " " + sizes[i];
}

function paginate(node,type,circle_id,is_faculty)
{
    var panel;
    if(type=='files') panel=document.getElementById('file-panel');
    else if(type=='notice') panel=document.getElementById('notice-panel');

    var current=node.parentNode.getElementsByTagName('input')[0];
    var prev=node.parentNode.getElementsByTagName('button')[0];
    var next=node.parentNode.getElementsByTagName('button')[1];

    var target=(node==prev)?current.value-5:current.value+5;

    $.ajax
    ({
        url: '/paginate',
        type: "post",
        data:
        {
            'type': type,
            'circle_id': circle_id,
            'target': target
        },
        success: function(data)
        {
            prev.disabled=(data[0]=='first');
            next.disabled=(data[1]=='last');
            current.value=target;
            while(panel.children.length>1) panel.children[0].remove();

            if(type=='files')
            {
                for(var i=2; i<data.length; i++)
                {
                    var id=data[i]['file_id'], name=data[i]['name'], size=data[i]['size'], comment=data[i]['comment'];

                    if(is_faculty==1) $remove='<small onclick="removeCircleContent(this.parentNode,\''+id+'\',\'file\')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>';
                    else $remove='';

                    var htmlcontent=
                        $remove+
                        '<b>File: </b><a style="color: #014c8c" href="/files/'+id+'/'+name+'">'+name+'</a><br>'+
                        '<b>Size: </b>'+formatBytes(size)+'<br>'+
                        '<b>Comment: </b>'+comment+
                        '<hr>';

                    elem=document.createElement("div");
                    elem.innerHTML=htmlcontent;

                    panel.insertBefore(elem,panel.lastElementChild);
                }
            }
            else if(type=='notice')
            {
                for(var i=2; i<data.length; i++)
                {
                    var notice_id=data[i]['notice_id'], topic=data[i]['topic'], description=data[i]['description'], created_at=data[i]['created_at'];

                    if(is_faculty==1) $remove='<small onclick="removeCircleContent(this.parentNode,\''+notice_id+'\',\'notice\')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>';
                    else $remove='';

                    var htmlcontent= 
                        $remove+                      
                        '<center><b>'+topic+'</b></center>'+
                        '<pre style="white-space: pre-wrap; font-family: blupi-font; font-size: 100%; color: #000;">'+description+'</pre>'+
                        '<small>'+created_at+'</small>'+
                        '<hr>';

                    var elem=document.createElement("div");
                    elem.innerHTML=htmlcontent;

                    panel.insertBefore(elem,panel.lastElementChild);
                }
            }
        }
    });
}

