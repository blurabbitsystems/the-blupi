<!DOCTYPE html>
<html lang="en">

<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/script.js"></script>
</head>

<body class="blupi-main">
    <header class="blupi-header">

        <div class="col-md-1 blupi-avatar">
            <img src="/images/avatar.jpg" class="img-circle" width="90" height="90">
        </div>

        <div class="col-md-8">
            <nav>
                <ul class="blupi-nav">
                    <li><a href="/admin/home">Home</a></li>
                    <li><a href="/admin/config">Config</a></li>
                    <li><a href="/admin/logout">Logout</a></li>
                </ul>
            </nav>
        </div>

    </header>

    <div class="container">

        <div class="row" style="min-height: 600px">
            <div class="col-md-2 box"></div>
            <div class="col-md-8 box">
                    
                <div id="blupi-new-post" class="blupi-sidebar-header blupi-new-post">
                    <div class="text-md-center" style="font-size: 120%">
                        
                        @yield('Admin-name')

                    </div>
                </div>

                @if(session()->has('status'))
                    <div style="padding: 5px 15px; margin-bottom: 8px"
                        class="alert alert-{{ session('status') }} fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ session('msg') }}</strong>
                    </div>
                @endif

                <div class="blupi-body" style="min-height: 600px;">
                    <div>
                        
                        @yield('Misc-body')
                        
                    </div>
                </div>

            </div>

            <div class="col-md-2 box"></div>
        </div>

    </div>

    <footer class="blupi-footer text-md-center">
        <div class="blupi-footer-nav">
            <a href="">Home</a> | <a href="">Logout</a>
        </div>
        <div class="blupi-footer-copyright">
            Copyright &copy; 2016
        </div>
    </footer>



    <!-- Modal for adding people to circle -->
    <div class="modal fade" id="addPeople" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Add people to this circle</h4>
                </div>
                <div class="modal-body">

                    <form method="post" action="/enroll_individual" class="form-control">

                        {!! csrf_field() !!}

                        <div style="height: 5px"></div>

                        <input name="circle_id" type="text"
                            class="form-control form-control-sm"
                            value="@yield('Circle-id')" readonly>

                        <div style="height: 8px"></div>

                        <strong>Username / Student no:</strong>
                        <input name="username" type="text" class="form-control form-control-sm" autocomplete="off" required>

                        <div style="height: 8px"></div>

                        <input type="submit" value="Go" class="btn btn-primary btn-sm">

                        <div style="height: 5px"></div>
                    </form>

                </div>
                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

</body>

<script type="text/javascript">
$.ajaxSetup
({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>

</html>
