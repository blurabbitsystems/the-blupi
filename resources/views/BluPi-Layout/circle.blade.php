<!DOCTYPE html>
<html lang="en">

<head>
    <title>BluPi Circle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/script.js"></script>
</head>

<body class="blupi-main">
    <header class="blupi-header">

        <div class="col-md-1 blupi-avatar">
            <img src="/images/avatar.jpg" class="img-circle" width="90" height="90">
        </div>

        <div class="col-md-8">
            <nav>
                <ul class="blupi-nav">

                    @yield('Navigation')
                    
                </ul>
            </nav>
        </div>

        <div class="col-md-3">
            <form method="get" action="/searchresult" class="blupi-search-form">
                <input class="blupi-search-field" name="q" placeholder="Search..." autocomplete="off" />
            </form>
        </div>

    </header>

    <div class="container" style="min-height: 650px">

        <div class="row">
            <div class="col-md-8 box">

                @if(session()->has('status'))
                    <div style="padding: 5px 15px; margin-bottom: 8px"
                        class="alert alert-{{ session('status') }} fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ session('msg') }}</strong>
                    </div>
                @endif

                <div id="blupi-new-post" class="blupi-sidebar-header blupi-new-post">

                    <div class="text-md-center">

                        @yield('Circle-Name')

                    </div>

                    <textarea name="post_body" class="form-control blupi-post-field" placeholder="Write a new post..."></textarea>


                    <div width="100%" align="right">
                        <button class="btn btn-default blupi-post-button"
                                onclick="insertCirclePost({{$circle->id}},'Michael De Santa')">
                            Post it!
                        </button>
                    </div>

                </div>

                <hr>

                <div class="blupi-body" id="blupi-post">

                    @yield('Posts')

                </div>
            </div>

            <!--Sidebar-->
            <div class="col-md-4 box">

                <div>
                    <div class="text-md-center blupi-sidebar-header" style="margin-bottom: 5px">
                        Circles
                    </div>

                    @yield('Circles')

                </div>

                <hr>

                <div id="notice-board">

                    @if($user->people->is_faculty==1)
                        <div class="blupi-sidebar-item blupi-sidebar-form-toggle" align="right">
                            <button onclick="showSidebarForm('notice-board',{{$circle->id}})" style="margin-right: 10px; background: transparent">
                                <img src="/images/insert-icon.png" alt="" width="25" height="25">
                            </button>
                        </div>
                    @endif

                    <div class="text-md-center blupi-sidebar-header" style="margin-bottom: 5px">
                        Notice Board
                    </div>

                    <div id="notice-panel" class="blupi-sidebar-body" style="margin-bottom: 5px">
                        
                        @yield('Notices')

                    </div>

                </div>

                <hr>

                <div id="file-explorer">

                    @if($user->people->is_faculty==1)
                        <div class="blupi-sidebar-item blupi-sidebar-form-toggle" align="right">
                            <button onclick="showSidebarForm('file-explorer',{{$circle->id}})" style="margin-right: 10px; background: transparent">
                                <img src="/images/insert-icon.png" alt="" width="25" height="25">
                            </button>
                        </div>
                    @endif

                    <div class="text-md-center blupi-sidebar-header" style="margin-bottom: 5px">
                        File Explorer
                    </div>

                    <div id="file-panel" class="blupi-sidebar-body" style="margin-bottom: 5px">
                        
                        @yield('Files')

                    </div>

                </div>

                <hr>

                <div id="class-test">
                    @if($user->people->is_faculty==1)
                        <div class="blupi-sidebar-item blupi-sidebar-form-toggle" align="right">
                            <button onclick="showSidebarForm('class-test',{{$circle->id}})" style="margin-right: 10px; background: transparent">
                                <img src="/images/insert-icon.png" alt="" width="25" height="25">
                            </button>
                        </div>
                    @endif

                    <div class="text-md-center blupi-sidebar-header" style="margin-bottom: 5px">
                        Class Test
                    </div>

                    <div id="ct-panel" class="blupi-sidebar-body" style="margin-bottom: 5px">
                        
                        @yield('Classtests')

                    </div>

                </div>

                <hr>

                <div id="assignment">
                    @if($user->people->is_faculty==1)
                        <div class="blupi-sidebar-item blupi-sidebar-form-toggle" align="right">
                            <button onclick="showSidebarForm('assignment',{{$circle->id}})" style="margin-right: 10px; background: transparent">
                                <img src="/images/insert-icon.png" alt="" width="25" height="25">
                            </button>
                        </div>
                    @endif

                    <div class="text-md-center blupi-sidebar-header" style="margin-bottom: 5px">
                        Assignment
                    </div>

                    <div id="assignment-panel" class="blupi-sidebar-body" style="margin-bottom: 5px">
                        
                        @yield('Assignments')

                    </div>

                </div>

            </div>
        </div>

    </div>

    <footer class="blupi-footer text-md-center">
        <div class="blupi-footer-nav">
            <a href="">Home</a> | <a href="">Logout</a>
        </div>
        <div class="blupi-footer-copyright">
            Copyright &copy; 2016
        </div>
    </footer>

    <!-- Modal for showing notifications -->
    <div class="modal fade" id="showNotifications" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Notifications</h4>
                </div>

                <div class="modal-body" id="notifications_panel">
                    
                </div>

                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>

</body>

<script type="text/javascript">
$.ajaxSetup
({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>

</html>
