<!DOCTYPE html>
<html lang="en">

<head>
    <title>BluPi home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/script.js"></script>
</head>

<body class="blupi-main">
    <header class="blupi-header text-md-center">
        <img class="blupi-logo" src="/images/blupi-logo-shadowed.png" alt="">
    </header>

    <div class="container" align="center" style="min-height: 600px">
        <hr>
        <div class="row blupi-login-header">
        
            @yield('Institution-Name')
            
        </div>

        <hr>

        <form class="form-signin" method="post" action="/admin_authentication">

            {!! csrf_field() !!}

            <div class="blupi-login-header" style="font-size: 24px; padding: 10px">Admin Login:</div>

            <label for="blupi-username" class="sr-only">Username</label>
            <input name="username" value="{{old('username')}}" type="text" id="blupi-username" class="form-control" placeholder="Username" autofocus required>

            <label for="blupi-password" class="sr-only">Password</label>
            <input name="password" type="password" id="blupi-password" class="form-control" placeholder="Password" required>

            <button class="btn btn-lg btn-primary btn-block" type="submit">
                Login
            </button>

            <div style="height: 10px"></div>

            @if(session()->has('status'))
                <div style="padding: 5px 15px; margin-bottom: 8px"
                    class="alert alert-{{ session('status') }} fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>{{ session('msg') }}</strong>
                </div>
            @endif

        </form>


    </div>

    <footer class="blupi-footer text-md-center">
        <div class="blupi-footer-nav">
            <a href="#">SkyBlu Systems</a> | <a href="#"> @yield('Institution-Name') </a>
        </div>
        <div class="blupi-footer-copyright">
            Copyright &copy; 2016
        </div>
    </footer>
</body>
</html>
