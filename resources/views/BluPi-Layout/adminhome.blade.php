<!DOCTYPE html>
<html lang="en">

<head>
    <title>Admin home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/script.js"></script>
</head>

<body class="blupi-main">
    <header class="blupi-header">

        <div class="col-md-1 blupi-avatar">
            <img src="/images/avatar.jpg" class="img-circle" width="90" height="90">
        </div>

        <div class="col-md-8">
            <nav>
                <ul class="blupi-nav">
                    <li class="active"><a href="/admin/home">Home</a></li>
                    <li><a href="/admin/config">Config</a></li>
                    <li><a href="/admin/logout">Logout</a></li>
                </ul>
            </nav>
        </div>

    </header>

    <div class="container">

        <div class="row" style="min-height: 600px">
            <div class="col-md-2 box"></div>

            <div class="col-md-8 box">

                <div id="blupi-new-post" class="blupi-sidebar-header blupi-new-post">
                    <div class="text-md-center" style="font-size: 120%">
                        
                        @yield('Admin-name')
                        
                    </div>
                </div>

                @if(session()->has('status'))
                    <div style="padding: 5px 15px; margin-bottom: 8px"
                        class="alert alert-{{ session('status') }} fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ session('msg') }}</strong>
                    </div>
                @endif

                <div class="blupi-body" style="min-height: 600px;">
                    <div>
                        <div class="col-md-12" style="padding: 2px 3px 4px 2px">
                            <div class="blupi-admin-content-header">
                                Institution name
                            </div>

                            <div class="blupi-admin-content-body" align="center" style="font-size: 125%">

                                @if($user->username=='root')
                                    <button style="float: right; padding: 0; margin: 0; background: none; border: 0"
                                        data-target="#setInstitutionName" data-toggle="modal">
                                        <img src="/images/edit-icon.png"
                                             height="20px" width="20px">
                                    </button>
                                @endif
                                
                                @yield('Institution-name')

                            </div>

                            <div style="height: 6px"></div>

                            <div class="blupi-admin-content-header">
                                Current Session
                            </div>

                            <div class="blupi-admin-content-body" align="center" style="font-size: 125%">

                                @if($user->username=='root')
                                    <button style="float: right; padding: 0; margin: 0; background: none; border: 0"
                                        data-target="#setSession" data-toggle="modal">
                                        <img src="/images/edit-icon.png"
                                             height="20px" width="20px">
                                    </button>
                                @endif

                                @yield('Session')

                            </div>
                        </div>

                        @yield('Upper-content')

                    </div>

 <!-- --- -->       @if ($user->username=='root' || $user->people->department_id!=NULL)

                    <div>
                        <div class="col-md-6" style="padding: 2px 3px 2px 2px">

                            <div class="blupi-admin-content-header">
                                
                                @yield('Left-header')

                            </div>

                            <div class="blupi-admin-content-body">
                                
                                @yield('Left-body')

                            </div>
                        </div>

                        <div class="col-md-6" style="padding: 2px 2px 2px 3px">
                            <div class="blupi-admin-content-header">
                                
                                @yield('Right-header')

                            </div>

                            <div class="blupi-admin-content-body">
                                
                                @yield('Right-body')

                            </div>
                        </div>
                    </div>

<!-- --- -->        @endif

                </div>

            </div>

            <div class="col-md-2 box">
            </div>
        </div>

    </div>

    <footer class="blupi-footer text-md-center">
        <div class="blupi-footer-nav">
            <a href="/admin/home">Home</a> | <a href="/admin/logout">Logout</a>
        </div>
        <div class="blupi-footer-copyright">
            Copyright &copy; 2016
        </div>
    </footer>

    <!-- Modal for Adding an admin -->
    <div class="modal fade" id="addAdmin" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Add an admin</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="/insert_admin" class="form-control">

                        {!! csrf_field() !!}

                        <strong>Full name:</strong>
                        <input name="name" type="text" required="true" autocomplete="off" class="form-control form-control-sm">

                        <strong>Username:</strong>
                        <input name="username" type="text" required="true" autocomplete="off" class="form-control form-control-sm">

                        <strong>Password:</strong>
                        <input name="password" type="text" required="true" autocomplete="off" class="form-control form-control-sm">

                        <input type="submit" value="Submit" class="btn btn-primary" style="margin-top: 8px">
                    </form>
                </div>
                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal for Adding a department -->
    <div class="modal fade" id="addDepartment" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Insert a department</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="/insert_department" class="form-control">

                        {!! csrf_field() !!}

                        <strong>Full name:</strong>
                        <input name="fullname" autocomplete="off" required="true" type="text" class="form-control form-control-sm">

                        <strong>Codename:</strong>
                        <input name="codename" autocomplete="off" required="true" type="text" class="form-control form-control-sm">

                        <input type="submit" value="Submit" class="btn btn-primary" style="margin-top: 8px">
                    </form>
                </div>
                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal for inserting people -->
    <div class="modal fade" id="insertPeople" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Insert new people</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="/insert_people" class="form-control">

                        {!! csrf_field() !!}

                        <strong>Full name:</strong>
                        <input name="name" type="text" autocomplete="off" required="true"
                            class="form-control form-control-sm">

                        <div style="height: 8px"></div>

                        <div class="checkbox">
                            <label style="margin: 7px 0 0 7px">
                                <input name="is_faculty" type="checkbox">
                                <strong>Faculty member?</strong>
                            </label>
                        </div>

                        <strong>Student no / Username:</strong>
                        <input name="username" type="text" autocomplete="off" required="true"
                            class="form-control form-control-sm">

                        <div style="height: 8px"></div>

                        <strong>Level-Term (if student):</strong>
                        <select name="levelterm" class="form-control">
                            <option value="1">Level 1 Term 1</option>
                            <option value="2">Level 1 Term 2</option>
                            <option value="3">Level 2 Term 1</option>
                            <option value="4">Level 2 Term 2</option>
                            <option value="5">Level 3 Term 1</option>
                            <option value="6">Level 3 Term 2</option>
                            <option value="7">Level 4 Term 1</option>
                            <option value="8">Level 4 Term 2</option>
                        </select>

                        <div style="height: 8px"></div>

                        <strong>Password:</strong>
                        <input name="password" type="text" autocomplete="off" required="true" 
                            class="form-control form-control-sm">

                        <input type="submit" value="Submit" class="btn btn-primary btn-sm" style="margin-top: 8px">
                    </form>
                </div>
                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal for creating courses -->
    <div class="modal fade" id="createCourse" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Create a course</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="/insert_course" class="form-control">

                        {!! csrf_field() !!}

                        <strong>Course name:</strong>
                        <input name="name" type="text" class="form-control form-control-sm" autocomplete="off" required>
                        
                        <div style="height: 8px"></div>

                        <strong>Course code:</strong>
                        <input name="code" type="text" class="form-control form-control-sm" autocomplete="off" required>

                        <input type="submit" value="Submit" class="btn btn-primary btn-sm" style="margin-top: 8px">
                    </form>
                </div>
                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal for creating enrollments -->
    <div class="modal fade" id="createEnrollments" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Select level-term and courses</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="/create_enrollments" class="form-control">

                        {!! csrf_field() !!}

                        <strong>Available courses:</strong>
                        <select multiple name="course_id_list[]" class="form-control">
                            @yield('Course-list')
                        </select>

                        <div style="height: 5px"></div>
                        
                        <strong>Level-Term:</strong>
                        <select name="levelterm" class="form-control">
                            <option value="1">Level 1 Term 1</option>
                            <option value="2">Level 1 Term 2</option>
                            <option value="3">Level 2 Term 1</option>
                            <option value="4">Level 2 Term 2</option>
                            <option value="5">Level 3 Term 1</option>
                            <option value="6">Level 3 Term 2</option>
                            <option value="7">Level 4 Term 1</option>
                            <option value="8">Level 4 Term 2</option>
                        </select>

                        <input type="submit" value="Next" class="btn btn-primary btn-sm" style="margin-top: 8px">

                    </form>
                </div>
                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal for setting department -->
    <div class="modal fade" id="setDepartment" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Set department</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="/set_department"
                         class="form-control">

                        {!! csrf_field() !!}

                        Admin ID:
                        <input name="people_id" value=""
                        class="form-control" id="setDepartment_Input" readonly>

                        <div style="height: 8px"></div>

                        <select name="codename" class="form-control">
                            
                            @yield('Department-list')

                        </select>

                        <input type="submit" value="Next" class="btn btn-primary btn-sm" style="margin-top: 8px">

                    </form>
                </div>
                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal for changing institution name -->
    <div class="modal fade" id="setInstitutionName" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Set institution name</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="/set_institution_name"
                         class="form-control">

                        {!! csrf_field() !!}

                        <input name="name" value="" autocomplete="off" required="true"
                        maxlength="40" class="form-control" >

                        <input type="submit" value="Next" class="btn btn-primary btn-sm" style="margin-top: 8px">

                    </form>
                </div>
                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal for setting session -->
    <div class="modal fade" id="setSession" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Set session</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="/set_session"
                         class="form-control">

                        {!! csrf_field() !!}

                        <input name="session" value="@yield('Session')" autocomplete="off" required="true"
                        maxlength="40" class="form-control" >

                        <div style="height: 6px"></div>

                        <div class="checkbox form-control">
                            <label>
                                <input name="upgrade_students" type="checkbox">
                                <strong>Upgrade Student Database</strong>
                            </label>
                        </div>

                        <input type="submit" value="Next" class="btn btn-primary btn-sm">

                    </form>
                </div>
                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</body>

<script type="text/javascript">
$.ajaxSetup
({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>

</html>
