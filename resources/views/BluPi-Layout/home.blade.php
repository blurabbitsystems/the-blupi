<!DOCTYPE html>
<html lang="en">

<head>
    <title>BluPi Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/script.js"></script>
</head>

<body class="blupi-main">
    <header class="blupi-header">

        <div class="col-md-1 blupi-avatar">
            <img src="/images/avatar.jpg" class="img-circle" width="90" height="90">
        </div>

        <div class="col-md-8">
            <nav>
                <ul class="blupi-nav">
                    @yield('Navigation')
                </ul>
            </nav>
        </div>

        <div class="col-md-3">
            <form method="get" action="/searchresult" class="blupi-search-form">
                <input class="blupi-search-field" name="q" placeholder="Search..." autocomplete="off" />
            </form>
        </div>

    </header>

    <div class="container" style="min-height: 650px">

        <div class="row">
            <div class="col-md-8 box">

                @if(session()->has('status'))
                    <div style="padding: 5px 15px; margin-bottom: 8px"
                        class="alert alert-{{ session('status') }} fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ session('msg') }}</strong>
                    </div>
                @endif

                <div id="blupi-new-post" class="blupi-sidebar-header blupi-new-post">

                    <div class="text-md-center" style="font-size: 120%">
                        @yield('Institution-Name')
                    </div>

                    @if($user->people->is_faculty==1)

                        <textarea name="post_body" class="form-control blupi-post-field" placeholder="Write a new post..." maxlength="255"></textarea>

                        <div class="row">
                            <div class="col-md-7 checkbox" style=" font-size: 75%;">
                                <label style="margin: 7px 0 0 7px">
                                    <input name="comment_disabled" type="checkbox"> Disable Comment
                                </label>
                            </div>

                            <div class="col-md-5" width="100%" align="right">
                                <button class="btn btn-default blupi-post-button" data-toggle="modal" data-target="#setAudience">
                                    Audience
                                </button>

                                <button class="btn btn-default blupi-post-button"
                                        onclick="insertGeneralPost()">
                                    Post it!
                                </button>
                            </div>
                        </div>

                    @endif
                    
                </div>

                <hr>

                <div class="blupi-body" id="blupi-post">

                    @yield('Posts')

                </div>
            </div>

            <div class="col-md-4 box">

                <div>
                    <div class="text-md-center blupi-sidebar-header" style="margin-bottom: 5px">
                        Circles
                    </div>

                    @yield('Circles')

                </div>

                <hr>

                <div id="general-notice">

                
                    @if($user->people->is_faculty==1)

                        <div class="blupi-sidebar-item blupi-sidebar-form-toggle" align="right">
                            <button onclick="showSidebarForm('general-notice')" style="margin-right: 10px; background: transparent">
                                <img src="images/insert-icon.png" alt="" width="25" height="25">
                            </button>
                        </div>

                    @endif
                    
                    <div class="text-md-center blupi-sidebar-header" style="margin-bottom: 5px">
                        General Notices
                    </div>

                    <div class="blupi-sidebar-body" style="margin-bottom: 5px">
                        <div id="general-notice-body">
                            @yield('General-Notices')
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>

    <footer class="blupi-footer text-md-center">
        <div class="blupi-footer-nav">
            <a href="/home">Home</a> | <a href="/profile">Profile</a> | <a href="/inbox">Inbox</a> | <a href="/logout">Logout</a>
        </div>
        <div class="blupi-footer-copyright">
            Copyright &copy; 2016
        </div>
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="setAudience" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Who will see this?</h4>
                </div>
                <div class="modal-body">
                    <strong>Level-Term (if student):</strong>
                    <select name="levelterm" class="form-control c-select">
                        <option value="0">Any</option>
                        <option value="1">Level 1 Term 1</option>
                        <option value="2">Level 1 Term 2</option>
                        <option value="3">Level 2 Term 1</option>
                        <option value="4">Level 2 Term 2</option>
                        <option value="5">Level 3 Term 1</option>
                        <option value="6">Level 3 Term 2</option>
                        <option value="7">Level 4 Term 1</option>
                        <option value="8">Level 4 Term 2</option>
                    </select>
                </div>
                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal for showing notifications -->
    <div class="modal fade" id="showNotifications" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header blupi-sidebar-header" style="border-radius: 0px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Notifications</h4>
                </div>

                <div class="modal-body" id="notifications_panel">
                    
                </div>

                <div class="modal-footer blupi-sidebar-header" style="border-radius: 0">
                    <button type="button" class="btn btn-default blupi-post-button" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>

</body>

<script type="text/javascript">
$.ajaxSetup
({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>

</html>
