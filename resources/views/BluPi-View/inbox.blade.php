@extends('BluPi-Layout.misc')

@section('Navigation')
	<li><a href="/home">Home</a></li>
	<li><a href="/profile">Profile</a></li>
	<li class="active">
		<a href="/inbox">
			Inbox
			@if($newMsgCount!=0)
				<span class="badge" id="notification_count">{{$newMsgCount}}</span>
			@endif
		</a>
	</li>
	<li>
		<a style="cursor: pointer;" onclick="fetchNotifications()" data-toggle="modal" data-target="#showNotifications">
			Notifications

			@if($notificationCount!=0)
				<span class="badge" id="notification_count">{{$notificationCount}}</span>
			@endif

		</a>
	</li>
	<li><a href="/logout">Logout</a></li>
@stop


@section('Header')
	Inbox
@stop

@section('Circles')

	@foreach($circleList as $circle)
		<a href="/circle/{{$circle->id}}" style="text-decoration: none">
		    <div class="text-md-center blupi-sidebar-body blupi-sidebar-circle" style="margin-bottom: 5px">
		        {{$circle->course->code}} ({{$circle->session}})
		    </div>
		</a>
	@endforeach

@stop



@section('General-Notices')
     
     @foreach($noticeList as $notice)
		<div>
			@if($user->people->is_faculty==1)
				<small onclick="removeGeneralNotice(this.parentNode,{{$notice->id}})" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif

			<center><b>{{$notice->subject}}</b></center>
			<p> {{ $notice->body}}</p>
			<small>{{ $notice->created_at->setTimezone('+06:00')->format('h:i a, M d, Y')}}</small>
			<hr>
		</div>
     @endforeach

     <form method="post" onsubmit="fetchGeneralNotice(this); return false;">
     	<input type="hidden" value="5">
     	<div align="right">
     		<button type="submit" style="background: none; border: none; padding: 0; color: #833">
     			<small><strong>more..</strong></small>
     		</button>
     	</div>
     </form>
@stop



@section('Body')
	<div class="panel-group" id="accordion" style="margin-bottom: 5px">
		@foreach($messageList as $message)
			{{echoMessage($message,$user)}}
		@endforeach
	</div>

	<hr>

	<div align="center">
		<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#sendMessage">New message</button>
	</div>
@stop

@section('Send-Message-Header')
	Send a message
@stop

@section('Send-Message-Body-Partial')
	<input id="receiver_id" type="hidden" name="receiver_id" required>

	<strong>To:</strong>
	<input id="receiver_name" type="text" name="receiver_name" class="form-control form-control-sm" onkeyup="searchPeople(this.value)" autocomplete="off" required>

	<div id="search_people_result"></div>

	<div style="height: 8px"></div>
@stop


<?php 
function echoMessage($message,$user)
{
	$style="info";
	$seen="";

	if($message->receiver_id==$user->id)
	{
		$hodor="<i>From</i> ";
		$people=$message->people;

		if($message->is_seen==0)
		{
			$style="primary";
			$seen='onclick="setMessageSeen('.$message->id.')"';
		}
	}
	else
	{
		$hodor="<i>To</i> ";
		$people=$message->receiver;
	}

	$htmlcontent=
		'<div class="panel panel-'.$style.'">
			<div class="panel-heading" style="cursor: pointer" data-toggle="collapse" data-parent="#accordion" data-target="#message_'.$message->id.'" '.$seen.'>
				<h4 class="panel-title" >
					<a href="/profile/'.$people->id.'">'.$hodor.'<strong>'.$people->name.':</strong></a>
					'.$message->about.'
				</h4>
			</div>
			<div id="message_'.$message->id.'" class="panel-collapse collapse">
				<div class="panel-body" style="background: #CCC">
					<pre style="white-space: pre-wrap;">'.$message->msg.'</pre>
					<small>'.$message->created_at->setTimezone('+06:00')->format('h:i a, M d, Y').'</small>
				</div>
			</div>
		</div>';

	echo $htmlcontent;
}
?>