@extends('BluPi-Layout.misc')

@section('Navigation')
	<li><a href="/home">Home</a></li>
	<li class="active"><a href="/profile">Profile</a></li>
	<li>
		<a href="/inbox">
			Inbox
			@if($newMsgCount!=0)
				<span class="badge" id="notification_count">{{$newMsgCount}}</span>
			@endif
		</a>
	</li>
	<li>
		<a style="cursor: pointer;" onclick="fetchNotifications()" data-toggle="modal" data-target="#showNotifications">
			Notifications

			@if($notificationCount!=0)
				<span class="badge" id="notification_count">{{$notificationCount}}</span>
			@endif

		</a>
	</li>
	<li><a href="/logout">Logout</a></li>
@stop


@section('Header')
	Profile
@stop

@section('Circles')

	@foreach($circleList as $circle)
		<a href="/circle/{{$circle->id}}" style="text-decoration: none">
		    <div class="text-md-center blupi-sidebar-body blupi-sidebar-circle" style="margin-bottom: 5px">
		        {{$circle->course->code}} ({{$circle->session}})
		    </div>
		</a>
	@endforeach

@stop



@section('General-Notices')
     
    @foreach($noticeList as $notice)
		<div>
			@if($user->people->is_faculty==1)
				<small onclick="removeGeneralNotice(this.parentNode,{{$notice->id}})" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif

			<center><b>{{$notice->subject}}</b></center>
			<p> {{ $notice->body}}</p>
			<small>{{ $notice->created_at->setTimezone('+06:00')->format('h:i a, M d, Y')}}</small>
			<hr>
		</div>
     @endforeach

     <form method="post" onsubmit="fetchGeneralNotice(this); return false;">
     	<input type="hidden" value="5">
     	<div align="right">
     		<button type="submit" style="background: none; border: none; padding: 0; color: #833">
     			<small><strong>more..</strong></small>
     		</button>
     	</div>
     </form>
@stop




@section('Body')
	<div class="row">
		<div class="col-md-3"><img src="/images/avatar_{{$people->id}}.jpg" width="100%" height="auto"></div>
		<div class="col-md-9" style="padding-left: 0; font-family: blupi-font">
			<h3 style="color: #014c8c">
				<strong>
					{{$people->name}}
					@if($user->id!=$people->id)
						<img src="/images/mail-icon.png" width="25px" height="auto" class="blupi-mail-icon"
							data-toggle="modal" data-target="#sendMessage">
					@endif
				</strong>
			</h3>
			<h5>
				@if($people->is_faculty==1)
					@if($people->faculty->rank)
						{{$people->faculty->rank}},
					@else
						Faculty member,
					@endif
				@else
					Student (Level {{$people->student->level}} Term {{$people->student->term}}),
				@endif
			</h5>
			<h5>Department of {{$people->department->fullname}}.</h5>
		</div>
	</div>

	@if($people->is_faculty==1)
		<pre style="padding-top: 10px; padding-left: 5px; font-family: blupi-font; font-size: 100%">{{$people->faculty->additional_info}}</pre>
	@endif

	@if($user->id==$people->id)
		<hr>
		<div align="center"><a href="/editprofile" class="btn btn-sm btn-primary">Modify</a></div>
	@endif

@stop


@section('Send-Message-Header')
	Send message to <strong>{{$people->name}}</strong>
@stop

@section('Send-Message-Body-Partial')
	<input type="hidden" name="receiver_id" value="{{$people->id}}">
@stop