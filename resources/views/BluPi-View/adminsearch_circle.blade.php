@extends('BluPi-Layout.adminmisc')

@section('Admin-name')
	{{ $user->people->name }} [Admin]
@stop


@section('Misc-body')
	<div class="col-md-12" style="padding: 2px 3px 4px 2px">
        <div class="blupi-admin-content-header">

        	Circle list ({{ $circleList->count() }})

        </div>

        <div class="blupi-admin-content-body" align="center">

            <table class="table table-condensed">
		        <thead>
		            <tr>
		                <th>ID</th>
		                <th>CIRCLE NAME</th>
		               	<th>STUDENT COUNT</th>
		                <th>COURSE TEACHERS</th>
		                <th></th>
		            </tr>
		        </thead>
		        <tbody>
		            
            		@foreach($circleList as $circle)
	        			{{ echoCircle($circle) }}
	        		@endforeach

		        </tbody>

		    </table>

        </div>
    </div>
@stop


<?php

function echoCircle($circle)
{
	$temp=$circle->enrollments->where('is_admin',1);
	$course_teachers="";
	
	foreach($temp as $t)
	{
		if($course_teachers=="") $course_teachers.=$t->people->name;
		else $course_teachers.=", ".$t->people->name;
	}

	$htmlcontent=
	'<tr>
    	<td>'.$circle->id.'</td>
    	<td>
    		<a href="/admin/circle/'.$circle->id.'">
    			'.$circle->course->code.': '.$circle->course->name.' ('.$circle->session.')
    		</a>
    	</td>
    	<td>'.$circle->enrollments->where('is_admin',0)->count().'</td>
    	<td>'.$course_teachers.'</td>
    	<td>
    		<input type="button" value="X"
            class="btn btn-danger btn-sm" style="padding: 0 8px; opacity: 0.8"
            onclick="removeCircle(this,'.$circle->id.')">
    	</td>
    </tr>';

    echo $htmlcontent;
}
?>