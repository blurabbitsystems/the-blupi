@extends('BluPi-Layout.adminmisc')

@section('Admin-name')
	{{ $user->people->name }} [Admin]
@stop

@section('Circle-id')
{{ $circle->id }}
@stop

@section('Misc-body')

	<div class="col-md-12" style="padding: 2px 3px 4px 2px">
        <center><h5><strong>
            Displaying {{ $circle->course->code }} ({{$circle->session}}) enrollment:
        </strong></h5></center>

        <div class="blupi-admin-content-header">
            Course teachers
        </div>

        <div class="blupi-admin-content-body">
            <table class="table table-condensed">
                <thead>
                    <tr class="table-active">
                        <th>ID</th>
                        <th>FULL NAME</th>
                        <th>RANK</th>
                        <th>RESEARCH</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($circle->enrollments->where('is_admin',1) as $enrollment)
                        {{ echoFaculty($enrollment) }}
                    @endforeach
                </tbody>
            </table>
        </div>

        <div style="height: 10px"></div>

        <div class="blupi-admin-content-header">
            Students
        </div>

        <div class="blupi-admin-content-body">
            <table class="table table-condensed">
                <thead>
                <tr class="table-active">
                    <th>ID</th>
                    <th>FULL NAME</th>
                    <th>STUDENT NO</th>
                    <th>LEVEL-TERM</th>
                    <th>MAJOR-MINOR</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($circle->enrollments->where('is_admin',0) as $enrollment)
                        {{ echoStudent($enrollment) }}
                    @endforeach
                </tbody>
            </table>
        </div>

        <hr>
        
        <div align="center">
            <button class="btn btn-sm btn-primary" data-target="#addPeople" data-toggle="modal">
                Add People
            </button>
        </div>
    </div>
@stop


<?php 

function echoFaculty($enrollment)
{
    $people=$enrollment->people;

    $htmlcontent=
    '<tr>
        <td>'.$people->id.'</td>
        <td><a href="/admin/config/'.$people->id.'">'.$people->name.'</a></td>
        <td>'.$people->faculty->rank.'</td>
        <td>'.$people->faculty->research_area.'</td>
        <td>
            <input onclick="deEnroll(this,'.$enrollment->id.')" type="button" value="X"
                   class="btn btn-sm btn-danger" style="padding: 0 8px; opacity: 0.8">
        </td>
    </tr>';

    echo $htmlcontent;
}

function echoStudent($enrollment)
{
    $people=$enrollment->people;

    $htmlcontent=
    '<tr>
        <td>'.$people->id.'</td>
        <td><a href="/admin/config/'.$people->id.'">'.$people->name.'</a></td>
        <td>'.$people->student->student_no.'</td>
        <td>'.$people->student->level.'-'.$people->student->term.'</td>
        <td>'.$people->student->major.'-'.$people->student->minor.'</td>
        <td>
            <input onclick="deEnroll(this,'.$enrollment->id.')" type="button" value="X"
                   class="btn btn-sm btn-danger" style="padding: 0 8px; opacity: 0.8">
        </td>
    </tr>';

    echo $htmlcontent;
}

?>