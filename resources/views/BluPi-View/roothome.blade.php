@extends('BluPi-Layout.adminhome')


@section('Admin-name')
    {{ $user->people->name }}
@stop



@section('Institution-name')
	{{ $metainfo->where('key','Institution-name')->first()->value }}
@stop

@section('Session')
{{$metainfo->where('key','Session')->first()->value}}
@stop

@section('Upper-content')
    
@stop



@section('Left-header')
	Admins
@stop


@section('Left-body')
	<table class="table table-condensed">
        <thead>
            <tr>
                <th>ID</th>
                <th>FULL NAME</th>
                <th>DEPARTMENT</th>
                <th></th>
            </tr>
        </thead>
        <tbody>

            @foreach ($adminList as $admin)
                @if ($admin->user->username!='root')
                    {{ echoAdmin($admin) }}
                @endif
            @endforeach

        </tbody>

    </table>

    <div>
        <button class="btn btn-primary btn-sm"
                data-toggle="modal" data-target="#addAdmin">
            Add
        </button>
    </div>
@stop



@section('Right-header')
	Departments
@stop


@section('Right-body')
	<table class="table table-condensed">
        <thead>
        <tr>
            <th>ID</th>
            <th>CODE</th>
            <th>ADMINS</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

            @foreach ($departmentList as $department)
                {{ echoDepartment($department) }}
            @endforeach

        </tbody>

    </table>

    <div>
        <button class="btn btn-primary btn-sm"
                data-toggle="modal" data-target="#addDepartment">
            Add
        </button>
    </div>
@stop


@section('Department-list')
    <option value="">None</option>
    @foreach($departmentList as $department)
        <option value="{{ $department->codename }}">
            {{ $department->fullname }}
        </option>
    @endforeach
@stop


<?php

function echoAdmin($admin)
{
    if(!$admin->department)
    {
        $department='Not Set';
        $css='btn-warning';
    }
    else
    {
        $department=$admin->department->codename;
        $css='btn-success';
    }

    $htmlcontent=
    '<tr>
        <td>'.$admin->id.'</td>
        <td><a href=/admin/config/'.$admin->id.'>'.$admin->name.'</a></td>
        <td>
            <input type="button" value="'.$department.'"
            class="btn '.$css.' btn-sm" style="padding: 0 8px"
            data-target="#setDepartment" data-toggle="modal"
            onclick="setDepartmentID('.$admin->id.')">
        </td>
        <td>
            <input type="button" value="X"
            class="btn btn-danger btn-sm" style="padding: 0 8px; opacity: 0.8"
            onclick="removeAdmin(this,'.$admin->id.')">
        </td>
    </tr>';

    echo $htmlcontent;
}


function echoDepartment($department)
{
    $temp=$department->people->where('is_admin',1);

    if($temp->count()==0) $admins='<small><i>N/A</i></small>';
    else
    {
        $admins='';
        $first=true;
        foreach ($temp as $t)
        {
            if(!$first) $admins.=', ';
            $admins.=$t->name;
            $first=false;
        }
    }

    $htmlcontent=
    '<tr>
        <td>'.$department->id.'</td>
        <td>'.$department->codename.'</td>
        <td>'.$admins.'</td>
        <td>
            <input type="button" value="X"
                   class="btn btn-danger btn-sm" style="padding: 0 8px; opacity: 0.8"
                   onclick="removeDepartment(this,'.$department->id.')">
        </td>
    </tr>';

    echo $htmlcontent;
}
?>