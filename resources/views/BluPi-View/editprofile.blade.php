@extends('BluPi-Layout.misc')

@section('Navigation')
	<li><a href="/home">Home</a></li>
	<li class="active"><a href="/profile">Profile</a></li>
	<li><a href="/inbox">Inbox</a></li>
	<li>
		<a style="cursor: pointer;" onclick="fetchNotifications()" data-toggle="modal" data-target="#showNotifications">
			Notifications

			@if($notificationCount!=0)
				<span class="badge" id="notification_count">{{$notificationCount}}</span>
			@endif

		</a>
	</li>
	<li><a href="/logout">Logout</a></li>
@stop


@section('Header')
	Profile
@stop

@section('Circles')

	@foreach($circleList as $circle)
		<a href="circle.html" style="text-decoration: none">
		    <div class="text-md-center blupi-sidebar-body blupi-sidebar-circle" style="margin-bottom: 5px">
		        {{$circle->course->code}} ({{$circle->session}})
		    </div>
		</a>
	@endforeach

@stop


@section('General-Notices')
     
     @foreach($noticeList as $notice)
		<div>
			@if($user->people->is_faculty==1)
				<small onclick="removeGeneralNotice(this.parentNode,{{$notice->id}})" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif

			<center><b>{{$notice->subject}}</b></center>
			<p> {{ $notice->body}}</p>
			<small>{{ $notice->created_at->setTimezone('+06:00')->format('h:i a, M d, Y')}}</small>
			<hr>
		</div>
     @endforeach

     <form method="post" onsubmit="fetchGeneralNotice(this); return false;">
     	<input type="hidden" value="5">
     	<div align="right">
     		<button type="submit" style="background: none; border: none; padding: 0; color: #833">
     			<small><strong>more..</strong></small>
     		</button>
     	</div>
     </form>
@stop


@section('Body')
	<div class="row">
		<div class="col-md-3"><img src="/images/avatar.jpg" width="100%" height="auto"></div>
		<div class="col-md-9" style="padding-left: 0; font-family: blupi-font">
			<h3 style="color: #014c8c"><strong>{{$user->people->name}}</strong></h3>
			
			<h5>
				@if($user->people->is_faculty==1)
					Associate Professor,
				@else
					Level 1 Term 2 student,
				@endif
			</h5>

			<h5>Department of Geology & Mineral Science.</h5>
		</div>
	</div>

	<form method="post" action="/save_profile" style="padding-top: 10px; font-family: blupi-font" enctype="multipart/form-data">

		{!! csrf_field() !!}
		
		<strong>Choose profile picture:</strong><br>
		<input type="file" name="picture" accept="image/*">    	

		@if($user->people->is_faculty==1)

			<div style="width: 70%; margin-top: 10px">
				<strong>Additional information:</strong><br>
				<textarea name="additional_info" class="form-control" placeholder="Additional info" maxlength="255">{{$user->people->faculty->additional_info}}</textarea>
			</div>

		@endif

		<hr>

		<div align="center">
			<input type="submit" class="btn btn-sm btn-primary" value="Save" />
		</div>
	</form>

@stop