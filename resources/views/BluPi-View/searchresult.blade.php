@extends('BluPi-Layout.misc')

@section('Navigation')
	<li><a href="/home">Home</a></li>
	<li><a href="/profile">Profile</a></li>
	<li>
		<a href="/inbox">
			Inbox
			@if($newMsgCount!=0)
				<span class="badge" id="notification_count">{{$newMsgCount}}</span>
			@endif
		</a>
	</li>
	<li>
		<a style="cursor: pointer;" onclick="fetchNotifications()" data-toggle="modal" data-target="#showNotifications">
			Notifications

			@if($notificationCount!=0)
				<span class="badge" id="notification_count">{{$notificationCount}}</span>
			@endif

		</a>
	</li>
	<li><a href="/logout">Logout</a></li>
@stop


@section('Header')
	Search results
@stop

@section('Circles')

	@foreach($circleList as $circle)
		<a href="/circle/{{$circle->id}}" style="text-decoration: none">
		    <div class="text-md-center blupi-sidebar-body blupi-sidebar-circle" style="margin-bottom: 5px">
		        {{$circle->course->code}} ({{$circle->session}})
		    </div>
		</a>
	@endforeach

@stop



@section('General-Notices')
     
     @foreach($noticeList as $notice)
		<div>
			<small onclick="removeGeneralNotice(this.parentNode,{{$notice->id}})" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			<center><b>{{$notice->subject}}</b></center>
			<p> {{ $notice->body}}</p>
			<small>{{ $notice->created_at->setTimezone('+06:00')->format('h:i a, M d, Y')}}</small>
			<hr>
		</div>
     @endforeach

     <form method="post" onsubmit="fetchGeneralNotice(this); return false;">
     	<input type="hidden" value="5">
     	<div align="right">
     		<button type="submit" style="background: none; border: none; padding: 0; color: #833">
     			<small><strong>more..</strong></small>
     		</button>
     	</div>
     </form>
@stop



@section('Body')
	<h4 style="color: #014c8c; font-family: blupi-font"><strong>People found:</strong></h4>

	<div style="padding-left: 10px">
	@foreach($peopleResults as $people)
		<h5 style="font-family: blupi-font">
			<a href="/profile/{{$people->id}}"><strong>{{$people->name}}</strong></a>
			<span style="font-size: 70%">
				@if($people->is_faculty==0)
					Level {{$people->student->level}} Term {{$people->student->level}} student, {{$people->department->codename}}
				@else
					{{$people->faculty->rank}}, {{$people->department->codename}}
				@endif
			</span>
		</h5>
	@endforeach
	</div>

	<hr>
@stop
