@extends('BluPi-Layout.home')


@section('Navigation')
	<li class="active"><a href="/home">Home</a></li>
	<li><a href="/profile">Profile</a></li>
	<li>
		<a href="/inbox">
			Inbox
			@if($newMsgCount!=0)
				<span class="badge">{{$newMsgCount}}</span>
			@endif
		</a>
	</li>
	<li>
		<a style="cursor: pointer;" onclick="fetchNotifications()" data-toggle="modal" data-target="#showNotifications">
			Notifications

			@if($notificationCount!=0)
				<span class="badge" id="notification_count">{{$notificationCount}}</span>
			@endif

		</a>
	</li>
	<li><a href="/logout">Logout</a></li>
@stop


@section('Institution-Name')
	{{ $metainfo->where('key','Institution-name')->first()->value }}
@stop


@section('Posts')
	@foreach($postList as $post)
		{{ showPost($post,$user->id) }}
	@endforeach

	{!! $postList->render() !!}
@stop


@section('Circles')

	@foreach($circleList as $circle)
		<a href="/circle/{{$circle->id}}" style="text-decoration: none">
		    <div class="text-md-center blupi-sidebar-body blupi-sidebar-circle" style="margin-bottom: 5px">
		        {{$circle->course->code}} ({{$circle->session}})
		    </div>
		</a>
	@endforeach

@stop


@section('General-Notices')
     
     @foreach($noticeList as $notice)
		<div>
			@if($user->people->is_faculty==1)
				<small onclick="removeGeneralNotice(this.parentNode,{{$notice->id}})" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif

			<center><b>{{$notice->subject}}</b></center>
			<p> {{ $notice->body}}</p>
			<small>{{ $notice->created_at->setTimezone('+06:00')->format('h:i a, M d, Y')}}</small>
			<hr>
		</div>
     @endforeach

     <form method="post" onsubmit="fetchGeneralNotice(this); return false;">
     	<input type="hidden" value="5">
     	<div align="right">
     		<button type="submit" style="background: none; border: none; padding: 0; color: #833">
     			<small><strong>more..</strong></small>
     		</button>
     	</div>
     </form>
@stop



<?php

function showPost($post,$user_id)
{
	$name=$post->people->name;
	$date=$post->created_at->setTimezone('+06:00')->format('h:i a, M d, Y');	
	$body=$post->post_body;
	
    $commentList=$post->comments;
	$html_comments='';

	$more_comments='';

	if($commentList->count()>3)
	{
		$more_comments=
			'<div align="">
				<a href="/post/'.$post->id.'" style="color: #833"><small><strong>And '.($commentList->count()-3).' more comment(s)...</strong></small></a>
			</div>
			<hr>';
	}

	$commentList=$commentList->take(3);

	foreach($commentList as $comment)
	{
		$remove_comment='';

		if($user_id==$comment->people_id || $user_id==$post->people_id)
		{
			$remove_comment=
				'<br>
				<small style="cursor: pointer">
					[<span class="text-danger" onclick="removeGeneralComment('.$post->id.','.$comment->id.')">remove</span>]
				</small>';
		}

		$html_comments.=
			'<div id="blupi-post-'.$post->id.'-comment-'.$comment->id.'">

				<div class="blupi-comment-name">
					<a class="blupi-a" href="/profile/'.$comment->people_id.'">'.$comment->people->name.'</a> says:
				</div>

				<div class="blupi-comment-body">
					'.$comment->comment_body.'
					'.$remove_comment.'
				</div>

				<hr>
				
			</div>';
	}

	$comment_form='<div align="center"><small><strong>:::Commenting is disabled:::</strong></small></div>';

	if($post->comment_disabled==0)
	{
		$comment_form=
			'<div>
				<textarea name="comment" class="form-control blupi-comment-field" placeholder="Write a comment..." maxlength="255"></textarea>
				<button class="btn btn-default blupi-comment-button" onclick="insertGeneralComment('.$post->id.')">
					Submit
				</button>
			</div>';
	}
	
	$remove_post='';

	if($user_id==$post->people_id)
	{
		$remove_post=
			'<br>
			<small style="cursor: pointer">
				[<span class="text-danger" onclick="removeGeneralPost('.$post->id.')">remove</span>]
			</small>';
	}

	$htmlcontent=
		'<div class="blupi-post" id="blupi-post-'.$post->id.'">

			<div class="row blupi-post-header">

				<div class="col-md-1">
					<img src="/images/avatar_'.$post->people_id.'.jpg" class="img-circle" width="55" height="55">
				</div>

				<div class="col-md-11" style="padding-top: 5px">

					<div class="blupi-post-name">
						<a class="blupi-a" href="/profile/'.$post->people_id.'">'. $name .'</a>
					</div>

					<div class="blupi-post-date">
						'. $date .'
					</div>
		                
		            </button>
				</div>
			</div>

			<hr>

			<div class="blupi-post-body">
				'. $body .'
				'.$remove_post.'
			</div>

			<hr>

			<!--Comment section-->

			<div id="blupi-post-'.$post->id.'-comment">

				'.$html_comments.'
				'.$more_comments.'
				'.$comment_form.'

			</div>

		</div>
	';

	echo $htmlcontent;
}

?>

