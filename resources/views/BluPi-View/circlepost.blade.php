@extends('BluPi-Layout.circle')


@section('Navigation')
	<li><a href="/home">Home</a></li>
	<li><a href="/profile">Profile</a></li>
	<li>
		<a href="/inbox">
			Inbox
			@if($newMsgCount!=0)
				<span class="badge" id="notification_count">{{$newMsgCount}}</span>
			@endif
		</a>
	</li>
	<li>
		<a style="cursor: pointer;" onclick="fetchNotifications()" data-toggle="modal" data-target="#showNotifications">
			Notifications

			@if($notificationCount!=0)
				<span class="badge" id="notification_count">{{$notificationCount}}</span>
			@endif

		</a>
	</li>
	<li><a href="/logout">Logout</a></li>
@stop



@section('Circle-Name')
	{{$circle->course->code}}: {{$circle->course->name}}
@stop



@section('Posts')
	
	{{showPost($post,$user)}}

@stop



@section('Circles')

	@foreach($circleList as $cir)
		<a href="/circle/{{$cir->id}}" style="text-decoration: none">
		    <div class="text-md-center blupi-sidebar-body blupi-sidebar-circle" style="margin-bottom: 5px">
		        {{$cir->course->code}} ({{$cir->session}})
		    </div>
		</a>
	@endforeach

@stop



@section('Notices')
	@foreach($circle->notices->sortByDesc('created_at')->take(5) as $notice)
		<div>
			@if($user->people->is_faculty==1)
				<small onclick="removeCircleContent(this.parentNode,{{$notice->id}},'notice')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif

			<center><b>{{$notice->subject}}</b></center>
			<p> {{ $notice->details}}</p>
			<small>{{ $notice->created_at->setTimezone('+06:00')->format('h:i a, M d, Y')}}</small>
			<hr>
		</div>
     @endforeach

     @if($circle->notices->count()>5)

	    <div align="right">
	    	<input type="hidden" value="0">

	 		<button onclick="paginate(this,'notice',{{$circle->id}},{{$user->people->is_faculty}})" class="blupi-paginate" disabled="true">
	 			<small><b>prev</b></small>
	 		</button>
	 		.
	 		<button onclick="paginate(this,'notice',{{$circle->id}},{{$user->people->is_faculty}})" class="blupi-paginate">
	 			<small><b>next</b></small>
	 		</button>
	 	</div>

 	@endif

@stop



@section('Files')

	@foreach($circle->files->sortByDesc('created_at')->take(5) as $file)

	    <div>
	    	@if($user->people->is_faculty==1)
				<small onclick="removeCircleContent(this.parentNode,{{$file->id}},'file')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif

	        <b>File: </b><a style="color: #014c8c" href="/files/{{$file->id}}/{{$file->name}}">{{$file->name}}</a><br>
	        <b>Size: </b>{{formatBytes($file->size)}}<br>
	        <b>Comment: </b>{{$file->comment}}
	        <hr>
	    </div>

    @endforeach

    @if($circle->files->count()>5)

	    <div align="right">
	    	<input type="hidden" value="0">

	 		<button onclick="paginate(this,'files',{{$circle->id}},{{$user->people->is_faculty}})" class="blupi-paginate" disabled="true">
	 			<small><b>prev</b></small>
	 		</button>
	 		.
	 		<button onclick="paginate(this,'files',{{$circle->id}},{{$user->people->is_faculty}})" class="blupi-paginate">
	 			<small><b>next</b></small>
	 		</button>
	 	</div>

 	@endif
@stop



@section('Classtests')

	<?php $count=1; ?>

	@foreach($circle->classtests as $classtest)

		<div>
			@if($user->people->is_faculty==1)
				<small onclick="removeCircleContent(this.parentNode,{{$classtest->id}},'classtest')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif
	        <center><b>Class Test {{$count++}}</b></center>
	        <b>Syllabus:</b> <pre style="white-space: pre-wrap; font-family: blupi-font; font-size: 100%; color: #000;">{{$classtest->syllabus}}</pre>
	        <b>Schedule:</b> {{$classtest->schedule->format('h:i a, M d')}}
	        <hr>
	    </div>

	@endforeach

@stop


@section('Assignments')

    <?php $count=1; ?>

	@foreach($circle->assignments as $assignment)

		<div>
			@if($user->people->is_faculty==1)
				<small onclick="removeCircleContent(this.parentNode,{{$assignment->id}},'assignment')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif
	        <center><b>Assignment {{$count++}}</b></center>
		    <b>Description:</b> <pre style="white-space: pre-wrap; font-family: blupi-font; font-size: 100%; color: #000;">{{$assignment->details}}</pre>
		    <b>Deadline:</b> {{$assignment->deadline->format('h:i a, M d')}}<br>

		    <div style="height: 8px"></div>

		    @if($user->people->is_faculty==0)

		    	<?php $submission=$assignment->submissions->where('student_id',$user->id)->first(); ?>

		    	@if($submission)
		    		<b>Submitted: </b> <a style="color: #014c8c" href="/files/{{$submission->file->id}}/{{$submission->file->name}}">{{$submission->file->name}}</a><br>

		    		@if($assignment->deadline->lt($now))

			    		<form method="post" action="/upload_submission" enctype="multipart/form-data">
			    			<input type="hidden" name="file_id" value="{{$submission->file->id}}">
					    	<input type="hidden" name="circle_id" value="{{$circle->id}}">
					    	<input type="hidden" name="assignment_id" value="{{$assignment->id}}">
					    	<b>Resubmit:</b><input type="file" name="file" onchange="this.parentNode.submit()">
					    </form>

				    @endif

		    	@else

		    		@if($assignment->deadline->lt($now))

					    <form method="post" action="/upload_submission" enctype="multipart/form-data">
					    	<input type="hidden" name="file_id" value="0">
					    	<input type="hidden" name="circle_id" value="{{$circle->id}}">
					    	<input type="hidden" name="assignment_id" value="{{$assignment->id}}">
					    	<b>Submit: </b><input type="file" name="file" onchange="this.parentNode.submit()">
					    </form>

					@else
						<strong class="text-danger"><i>Not submitted.</i></strong>
					@endif

			    @endif

		    @else
		    	<a href="/circle/assignment/{{$assignment->id}}/submissions">See Submissions</a>
		    @endif

	        <hr>
	    </div>

	@endforeach

	
@stop



<?php
function formatBytes($size, $precision=2)
{
    $base = log($size, 1024);
    $suffixes = array('bytes', 'K', 'M');

    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}

function showPost($post,$user)
{
	$name=$post->people->name;
	$date=$post->created_at->setTimezone('+06:00')->format('h:i a, M d, Y');	
	$body=$post->post_body;
	
    $commentList=$post->comments;
	$html_comments="";

	foreach($commentList as $comment)
	{
		$remove_comment='';

		if($user->people->is_faculty==1 || $post->people_id==$user->id || $comment->people_id==$user->id)
		{
			$remove_comment=
				'<br>
				<small style="cursor: pointer">
					[<span class="text-danger" onclick="removeCircleComment('.$post->id.','.$comment->id.')">remove</span>]
				</small>';
		}

		$html_comments.=
			'<div id="blupi-post-'.$post->id.'-comment-'.$comment->id.'">

				<div class="blupi-comment-name">
					<a class="blupi-a" href="/profile/'.$comment->people_id.'">'.$comment->people->name.'</a> says:
				</div>

				<div class="blupi-comment-body">
					'.$comment->comment_body.'
					'.$remove_comment.'
				</div>

				<hr>
				
			</div>';
	}

	$remove_post='';

	if($user->people->is_faculty==1 || $post->people_id==$user->id)
	{
		$remove_post=
			'<small style="cursor: pointer">
				[<span class="text-danger" onclick="removeCirclePost('.$post->id.')">remove</span>]
			</small>';
	}

	$htmlcontent='
		<div class="blupi-post" id="blupi-post-'.$post->id.'">

			<div class="row blupi-post-header">

				<div class="col-md-1">
					<img src="/images/avatar_'.$post->people_id.'.jpg" class="img-circle" width="55" height="55">
				</div>

				<div class="col-md-11" style="padding-top: 5px">

					<div class="blupi-post-name">
						<a class="blupi-a" href="/profile/'.$post->people_id.'">'. $name .'</a>
					</div>

					<div class="blupi-post-date">
						'. $date .'
					</div>
		                
		            </button>
				</div>
			</div>

			<hr>

			<div class="blupi-post-body">
				'.$body.'<br>
				'.$remove_post.'
			</div>

			<hr>

			<!--Comment section-->

			<div id="blupi-post-'.$post->id.'-comment">

				'.$html_comments.'

				<!--Comment form-->

				<div>
					<textarea name="comment" class="form-control blupi-comment-field" placeholder="Write a comment..."></textarea>
					<button class="btn btn-default blupi-comment-button" onclick="insertCircleComment('.$post->id.')">
						Submit
					</button>
				</div>
			</div>

		</div>
	';

	echo $htmlcontent;
}

?>


