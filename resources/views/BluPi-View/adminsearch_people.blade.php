@extends('BluPi-Layout.adminmisc')

@section('Admin-name')
	{{ $user->people->name }} [Admin]
@stop


@section('Misc-body')
	<div class="col-md-12" style="padding: 2px 3px 4px 2px">
        <div class="blupi-admin-content-header">

        	@if($is_faculty==0)
        		Student list ({{ $studentList->count() }})
        	@else
        		Faculty list ({{ $facultyList->count() }})
        	@endif

        </div>

        <div class="blupi-admin-content-body" align="center">

            <table class="table table-condensed">
		        <thead>
		            <tr>
		                <th>ID</th>
		                <th>FULL NAME</th>
		                <th>
		                	@if($is_faculty==0)
				        		STUDENT NO
				        	@else
				        		USERNAME
				        	@endif
		                </th>
		               	<th>
		               		@if($is_faculty==0)
				        		LEVEL-TERM
				        	@else
				        		RANK
				        	@endif
		               	</th>
		               	@if($is_faculty==0)
		               		<th>MAJOR-MINOR</th>
		               	@endif
		                <th></th>
		            </tr>
		        </thead>
		        <tbody>
		            
	            	@if($is_faculty==0)
	            		@foreach($studentList as $student)
		        			{{ echoStudent($student) }}
		        		@endforeach
		        	@else
		        		@foreach($facultyList as $faculty)
		        			{{ echoFaculty($faculty) }}
		        		@endforeach
		        	@endif

		        </tbody>

		    </table>

        </div>
    </div>
@stop


<?php

function echoFaculty($faculty)
{
	$htmlcontent=
	'<tr>
    	<td>'.$faculty->people->id.'</td>
    	<td><a href="/admin/config/'.$faculty->people->id.'">'.$faculty->people->name.'</a></td>
    	<td>'.$faculty->people->user->username.'</td>
    	<td>'.$faculty->rank.'</td>
    	<td>
    		<input type="button" value="X"
            class="btn btn-danger btn-sm" style="padding: 0 8px; opacity: 0.8"
            onclick="removePeople(this,'.$faculty->id.')">
    	</td>
    </tr>';

    echo $htmlcontent;
}

function echoStudent($student)
{
	$htmlcontent=
	'<tr>
    	<td>'.$student->id.'</td>
    	<td><a href="/admin/config/'.$student->id.'">'.$student->people->name.'</a></td>
    	<td>'.$student->student_no.'</td>
    	<td>'.$student->level.'-'.$student->term.'</td>
    	<td>'.'N/A'.'</td>
    	<td>
    		<input type="button" value="X"
            class="btn btn-danger btn-sm" style="padding: 0 8px; opacity: 0.8"
            onclick="removePeople(this,'.$student->id.')">
    	</td>
    </tr>';

    echo $htmlcontent;
}
?>