@extends('BluPi-Layout.adminmisc')

@section('Admin-name')
	{{ $user->people->name }}
    @if ($user->username!='root')
        [Admin]
    @endif
@stop

@section('Misc-body')

    <div class="blupi-admin-content-header">
        Fullname
    </div>

    <div class="blupi-admin-content-body text-md-center">
        {{ $people->name }}
    </div>

    <div style="height: 8px"></div>
    
    <div class="blupi-admin-content-header">
        Username
    </div>

    <div class="blupi-admin-content-body text-md-center">
        {{ $people->user->username }}
    </div>

    @if($people->is_admin==0)
        @if($people->is_faculty==0)
            <div style="height: 8px"></div>

            <div class="blupi-admin-content-header">
                Level-Term
            </div>

            <div class="blupi-admin-content-body text-md-center">
                {{$people->student->level}}-{{$people->student->term}}
            </div>

            <div style="height: 8px"></div>

            <div class="blupi-admin-content-header">
                Major-Minor
            </div>

            <div class="blupi-admin-content-body text-md-center">
                {{$people->student->major}}-{{$people->student->minor}}
            </div>
        @else
            <div style="height: 8px"></div>

            <div class="blupi-admin-content-header">
                Rank
            </div>

            <div class="blupi-admin-content-body text-md-center">
                {{$people->faculty->rank?$people->faculty->rank:'-'}}
            </div>

            <div style="height: 8px"></div>

            <div class="blupi-admin-content-header">
                Research area
            </div>

            <div class="blupi-admin-content-body text-md-center">
                {{$people->faculty->research_area?$people->faculty->research_area:'-'}}
            </div>
        @endif
    @endif

    <hr>

    <div class="text-md-center">
        <a href="/admin/editconfig/{{$people->id}}" class="btn btn-primary btn-sm">Modify</a>
    </div>
    
@stop