@extends('BluPi-Layout.circle')


@section('Navigation')
	<li><a href="/home">Home</a></li>
	<li><a href="/profile">Profile</a></li>
	<li>
		<a href="/inbox">
			Inbox
			@if($newMsgCount!=0)
				<span class="badge" id="notification_count">{{$newMsgCount}}</span>
			@endif
		</a>
	</li>
	<li>
		<a style="cursor: pointer;" onclick="fetchNotifications()" data-toggle="modal" data-target="#showNotifications">
			Notifications

			@if($notificationCount!=0)
				<span class="badge" id="notification_count">{{$notificationCount}}</span>
			@endif

		</a>
	</li>
	<li><a href="/logout">Logout</a></li>
@stop



@section('Circle-Name')
	{{$circle->course->code}}: {{$circle->course->name}}
@stop



@section('Posts')
	
	<h4 style="color: #014c8c; font-family: blupi-font; padding-top: 8px"><strong>Submissions:</strong></h4>

	<table class="table table-condensed">
        <thead>
            <tr>
                <th>Student No</th>
                <th>Submission</th>
            </tr>
        </thead>
        <tbody>

        	@foreach($assignment->submissions as $submission)

            <tr>
            	<td><a style="color: #014c8c" href="/profile/{{$submission->student->id}}">{{$submission->student->student_no}}</a>
            	</td>
            	<td><a style="color: #014c8c" href="/files/{{$submission->file->id}}/{{$submission->file->name}}">{{$submission->file->name}}</a></td>
            </tr>

            @endforeach
        </tbody>

    </table>

    <hr>

@stop



@section('Circles')

	@foreach($circleList as $cir)
		<a href="/circle/{{$cir->id}}" style="text-decoration: none">
		    <div class="text-md-center blupi-sidebar-body blupi-sidebar-circle" style="margin-bottom: 5px">
		        {{$cir->course->code}} ({{$cir->session}})
		    </div>
		</a>
	@endforeach

@stop



@section('Notices')
	@foreach($circle->notices->sortByDesc('created_at')->take(5) as $notice)
		<div>
			@if($user->people->is_faculty==1)
				<small onclick="removeCircleContent(this.parentNode,{{$notice->id}},'notice')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif

			<center><b>{{$notice->subject}}</b></center>
			<p> {{ $notice->details}}</p>
			<small>{{ $notice->created_at->setTimezone('+06:00')->format('h:i a, M d, Y')}}</small>
			<hr>
		</div>
     @endforeach

     @if($circle->notices->count()>5)

	    <div align="right">
	    	<input type="hidden" value="0">

	 		<button onclick="paginate(this,'notice',{{$circle->id}},{{$user->people->is_faculty}})" class="blupi-paginate" disabled="true">
	 			<small><b>prev</b></small>
	 		</button>
	 		.
	 		<button onclick="paginate(this,'notice',{{$circle->id}},{{$user->people->is_faculty}})" class="blupi-paginate">
	 			<small><b>next</b></small>
	 		</button>
	 	</div>

 	@endif

@stop



@section('Files')

	@foreach($circle->files->sortByDesc('created_at')->take(5) as $file)

	    <div>
	    	@if($user->people->is_faculty==1)
				<small onclick="removeCircleContent(this.parentNode,{{$file->id}},'file')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif

	        <b>File: </b><a style="color: #014c8c" href="/files/{{$file->id}}/{{$file->name}}">{{$file->name}}</a><br>
	        <b>Size: </b>{{formatBytes($file->size)}}<br>
	        <b>Comment: </b>{{$file->comment}}
	        <hr>
	    </div>

    @endforeach

    @if($circle->files->count()>5)

	    <div align="right">
	    	<input type="hidden" value="0">

	 		<button onclick="paginate(this,'files',{{$circle->id}},{{$user->people->is_faculty}})" class="blupi-paginate" disabled="true">
	 			<small><b>prev</b></small>
	 		</button>
	 		.
	 		<button onclick="paginate(this,'files',{{$circle->id}},{{$user->people->is_faculty}})" class="blupi-paginate">
	 			<small><b>next</b></small>
	 		</button>
	 	</div>

 	@endif
@stop



@section('Classtests')

	<?php $count=1; ?>

	@foreach($circle->classtests as $classtest)

		<div>
			@if($user->people->is_faculty==1)
				<small onclick="removeCircleContent(this.parentNode,{{$classtest->id}},'classtest')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif
	        <center><b>Class Test {{$count++}}</b></center>
	        <b>Syllabus:</b> <pre style="white-space: pre-wrap; font-family: blupi-font; font-size: 100%; color: #000;">{{$classtest->syllabus}}</pre>
	        <b>Schedule:</b> {{$classtest->schedule->format('h:i a, M d')}}
	        <hr>
	    </div>

	@endforeach

@stop


@section('Assignments')

    <?php $count=1; ?>

	@foreach($circle->assignments as $assignment)

		<div>
			@if($user->people->is_faculty==1)
				<small onclick="removeCircleContent(this.parentNode,{{$assignment->id}},'assignment')" style="cursor: pointer; color: #844; float: right"><b>[X]</b></small>
			@endif
	        <center><b>Assignment {{$count++}}</b></center>
		    <b>Description:</b> <pre style="white-space: pre-wrap; font-family: blupi-font; font-size: 100%; color: #000;">{{$assignment->details}}</pre>
		    <b>Deadline:</b> {{$assignment->deadline->format('h:i a, M d')}}<br>

		    <div style="height: 8px"></div>

		    @if($user->people->is_faculty==0)

		    	<?php $submission=$assignment->submissions->where('student_id',$user->id)->first(); ?>

		    	@if($submission)
		    		<b>Submitted: </b> <a style="color: #014c8c" href="/files/{{$submission->file->id}}/{{$submission->file->name}}">{{$submission->file->name}}</a><br>

		    		@if($assignment->deadline->lt($now))

			    		<form method="post" action="/upload_submission" enctype="multipart/form-data">
			    			<input type="hidden" name="file_id" value="{{$submission->file->id}}">
					    	<input type="hidden" name="circle_id" value="{{$circle->id}}">
					    	<input type="hidden" name="assignment_id" value="{{$assignment->id}}">
					    	<b>Resubmit:</b><input type="file" name="file" onchange="this.parentNode.submit()">
					    </form>

				    @endif

		    	@else

		    		@if($assignment->deadline->lt($now))

					    <form method="post" action="/upload_submission" enctype="multipart/form-data">
					    	<input type="hidden" name="file_id" value="0">
					    	<input type="hidden" name="circle_id" value="{{$circle->id}}">
					    	<input type="hidden" name="assignment_id" value="{{$assignment->id}}">
					    	<b>Submit: </b><input type="file" name="file" onchange="this.parentNode.submit()">
					    </form>

					@else
						<strong class="text-danger"><i>Not submitted.</i></strong>
					@endif

			    @endif

		    @else
		    	<a href="/circle/assignment/{{$assignment->id}}/submissions">See Submissions</a>
		    @endif

	        <hr>
	    </div>

	@endforeach

	
@stop



<?php
function formatBytes($size, $precision=2)
{
    $base = log($size, 1024);
    $suffixes = array('bytes', 'K', 'M');

    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}

?>


