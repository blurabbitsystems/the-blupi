@extends('BluPi-Layout.adminmisc')

@section('Admin-name')
	{{ $user->people->name }}
@stop

@section('Misc-body')
	<form method="post" action="/save_config/{{$people->id}}" class="form-horizontal">
		
		{!! csrf_field() !!}

		<div class="row">
			<div class="col-md-6">
				<span class="label label-primary">Full name:</span>
				<input name="fullname" type="text" class="form-control" value="{{$people->name}}" required>

				<div style="height: 8px"></div>

				<span class="label label-default">Username:</span>
				<input name="username" type="text" class="form-control" value="{{$people->user->username}}" readonly>

				@if($people->is_admin==0)
					@if($people->is_faculty==0)
						<div style="height: 8px"></div>

						<span class="label label-primary">Major:</span>
						<input name="major" type="text" class="form-control" value="{{$people->student->major}}">

						<div style="height: 8px"></div>

						<span class="label label-primary">Level-Term:</span>
						<select name="levelterm" class="form-control c-select">
				            {{ leveltermlist($people->student) }}
				        </select>
				    @else
				    	<div style="height: 8px"></div>

						<span class="label label-primary">Rank:</span>
						<select name="rank" class="form-control c-select">
				            {{ ranklist($people->faculty) }}
				        </select>
					@endif
				@endif

			</div>

			<div class="col-md-6">
				<span class="label label-primary">Password:</span>
				<input name="password" type="password" class="form-control">

				<div style="height: 8px"></div>

				<span class="label label-primary">Retype password:</span>
				<input name="password_retype" type="password" class="form-control">

				@if($people->is_admin==0)
					@if($people->is_faculty==0)
						<div style="height: 8px"></div>

						<span class="label label-primary">Minor:</span>
						<input name="minor" type="text" class="form-control" value="{{$people->student->minor}}">
					@else
						<div style="height: 8px"></div>

						<span class="label label-primary">Research area:</span>
						<input name="research_area" type="text" class="form-control" value="{{$people->faculty->research_area}}">
					@endif
				@endif
			</div>
		</div>

		<hr>

		<div align="center">
			<input type="submit" class="btn btn-sm btn-primary" value="Save">
		</div>
	</form>
@stop


<?php 
function leveltermlist($student)
{
	$htmlcontent='<option value=""></option>';

	for($l=1;$l<=4;$l++)
		for ($t=1;$t<=2;$t++)
		{
			$val=($l-1)*2+$t;
			if($student->level==$l && $student->term==$t)
				$htmlcontent.='<option value="'.$val.'" selected>Level '.$l.' Term '.$t.'</option>';
			else
				$htmlcontent.='<option value="'.$val.'">Level '.$l.' Term '.$t.'</option>';
		}

	echo $htmlcontent;

}

function ranklist($faculty)
{
	$ranks=["Processor","Associate Professor","Assistant Professor", "Lecturer"];

	$htmlcontent='<option value="">Unspecified</option>';

	foreach($ranks as $r)
	{
		if($r==$faculty->rank)
			$htmlcontent.='<option value="'.$r.'" selected>'.$r.'</option>';
		else
			$htmlcontent.='<option value="'.$r.'">'.$r.'</option>';
	}

	echo $htmlcontent;
}
?>