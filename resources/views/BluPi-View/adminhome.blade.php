@extends('BluPi-Layout.adminhome')


@section('Admin-name')
    {{ $user->people->name }} [Admin]
@stop

@section('Institution-name')
	{{ $metainfo->where('key','Institution-name')->first()->value }}
@stop

@section('Session')
{{$metainfo->where('key','Session')->first()->value}}
@stop


@section('Upper-content')
    <div class="col-md-12" style="padding: 2px 3px 4px 2px">

        <div class="blupi-admin-content-header">
            Authorized department
        </div>

        <div class="blupi-admin-content-body" align="center" style="font-size: 125%">

            @if ($user->people->department_id!=NULL)
                {{ $user->people->department->fullname }}
            @else
                <small style="color: red;">Not assigned yet</small>
            @endif
        </div>
    </div>

    @if ($user->people->department_id!=NULL)
        <div class="col-md-12" style="padding: 2px 3px 0px 2px">
            <div class="blupi-admin-content-header">
                Available courses
            </div>
            <div class="blupi-admin-content-body">
                @foreach($courseList as $course)
                    <label class="label label-default">{{ $course->code }}</label>
                @endforeach
            </div>
        </div>


        <div class="col-md-12" style="padding: 2px 3px 4px 2px">
            <hr>

            <div class="blupi-admin-content-header">
                Authorized operations
            </div>

            <div class="blupi-admin-content-body" align="center">
                <div style="margin: 3px 0">
                    <button class="btn btn-sm btn-primary"
                            data-target="#insertPeople" data-toggle="modal">
                        Insert new people
                    </button>
                    <button class="btn btn-sm btn-primary"
                            data-target="#createCourse" data-toggle="modal">
                        Create new course
                    </button>
                    <button class="btn btn-sm btn-primary"
                            data-target="#createEnrollments" data-toggle="modal">
                        Create enrollments
                    </button>
                </div>
            </div>

            <hr>
        </div>

    @endif
@stop



@section('Left-header')
	People
    ({{ $peopleList->count() }})
@stop


@section('Left-body')
	<form method="get" action="/admin/people">
        
        <strong>Student or faculty member?</strong>
        <select name="is_faculty" class="form-control c-select-sm">
            <option value="0">Student</option>
            <option value="1">Faculty</option>
        </select>

        <div style="height: 8px"></div>

        <strong>Level-Term (if student):</strong>
        <select name="levelterm" class="form-control c-select-sm">
            <option value="0">Any</option>
            <option value="1">Level 1 Term 1</option>
            <option value="2">Level 1 Term 2</option>
            <option value="3">Level 2 Term 1</option>
            <option value="4">Level 2 Term 2</option>
            <option value="5">Level 3 Term 1</option>
            <option value="6">Level 3 Term 2</option>
            <option value="7">Level 4 Term 1</option>
            <option value="8">Level 4 Term 2</option>
        </select>

        <div style="height: 8px"></div>

        <input type="submit" value="Go" class="btn btn-primary"
               style="padding: 0px 5px">

        <div style="height: 5px"></div>
    </form>
@stop



@section('Right-header')
	Circles
@stop


@section('Right-body')
    <form method="get" action="/admin/circles">

    	<strong>Course:</strong>
        <select name="course_id" class="form-control c-select-sm">
            <option value="0">All</option>

            @foreach($courseList as $course)
                <option value="{{$course->id}}">{{$course->code}}: {{$course->name}}</option>
            @endforeach

        </select>

        <div style="height: 8px"></div>

        <strong>Session:</strong>
        <input name="session" value="{{$metainfo->where('key','Session')->first()->value}}"
            type="text" class="form-control form-control-sm" autocomplete="off" required>

        <div style="height: 8px"></div>

        <input type="submit" value="Go" class="btn btn-primary" style="padding: 0px 5px">

        <div style="height: 5px"></div>
    </form>
@stop

@section('Course-list')
    @foreach($courseList as $course)
        <option value="{{$course->id}}">{{$course->code}}: {{$course->name}}</option>
    @endforeach
@stop