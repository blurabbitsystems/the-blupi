<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GeneralNotices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_notices', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('people_id')->unsigned();
            $table->string('subject',30);
            $table->string('body',150);
            $table->tinyinteger('scope_level')->default(0)->unsigned();
            $table->tinyinteger('scope_term')->default(0)->unsigned();
            $table->timestamps();
        });

        Schema::table('general_notices', function($table)
        {
            $table->foreign('people_id')->references('id')->on('peoples')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('general_notices');
    }
}
