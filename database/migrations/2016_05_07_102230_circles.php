<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Circles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circles', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('session',15);
            $table->integer('course_id')->unsigned();
            $table->integer('department_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('circles', function($table)
        {
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('circles');
    }
}
