<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Faculties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faculties', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('rank',30);
            $table->string('research_area',50);
            $table->string('additional_info');
            $table->timestamps();
        });

        Schema::table('faculties', function($table)
        {
            $table->foreign('id')->references('id')->on('peoples')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('faculties');
    }
}
