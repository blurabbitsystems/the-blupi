<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Enrollments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrollments', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('circle_id')->unsigned();
            $table->integer('people_id')->unsigned();
            $table->tinyinteger('is_admin')->default(0);
            $table->timestamps();
        });

        Schema::table('enrollments', function($table)
        {
            $table->foreign('people_id')->references('id')->on('peoples')->onDelete('cascade');
            $table->foreign('circle_id')->references('id')->on('circles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enrollments');
    }
}
