<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Students extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('student_no',15);
            $table->tinyinteger('level')->nullable();
            $table->tinyinteger('term')->nullable();
            $table->string('major',20);
            $table->string('minor',20);
            $table->timestamps();
        });

        Schema::table('students', function($table)
        {
            $table->foreign('id')->references('id')->on('peoples')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students');
    }
}
