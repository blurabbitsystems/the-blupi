<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Messages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('people_id')->unsigned();
            $table->integer('receiver_id')->unsigned();
            $table->string('about',50);
            $table->string('msg');
            $table->tinyinteger('is_seen')->default(0)->unsigned();
            $table->timestamps();
        });

        Schema::table('messages', function($table)
        {
            $table->foreign('people_id')->references('id')->on('peoples')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
