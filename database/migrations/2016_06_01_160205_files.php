<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Files extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('circle_id')->unsigned();
            $table->string('name',30);
            $table->integer('size')->unsigned();
            $table->string('mime',30);
            $table->binary('data');
            $table->string('comment',80);
            $table->tinyinteger('is_special')->default(0)->unsigned();
            $table->timestamps();
        });

        Schema::table('files', function($table)
        {
            $table->foreign('circle_id')->references('id')->on('circles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files');
    }
}
