<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Peoples extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peoples', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name',50);
            $table->binary('picture')->nullable();
            $table->integer('department_id')->nullable()->unsigned();
            $table->tinyinteger('is_faculty')->default(0)->unsigned();
            $table->tinyinteger('is_admin')->default(0)->unsigned();
            $table->timestamps();
        });

        Schema::table('peoples', function($table)
        {
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peoples');
    }
}

