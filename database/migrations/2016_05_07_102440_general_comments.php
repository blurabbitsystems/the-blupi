<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GeneralComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_comments', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('general_post_id')->unsigned();
            $table->integer('people_id')->unsigned();
            $table->string('comment_body');
            $table->timestamps();
        });

        Schema::table('general_comments', function($table)
        {
            $table->foreign('people_id')->references('id')->on('peoples')->onDelete('cascade');
            $table->foreign('general_post_id')->references('id')->on('general_posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('general_comments');
    }
}
