<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Assignments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('circle_id')->unsigned();
            $table->binary('details');
            $table->timestamp('deadline');
            $table->binary('evaluation')->nullable();
            $table->timestamps();
        });

        Schema::table('assignments', function($table)
        {
            $table->foreign('circle_id')->references('id')->on('circles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assignments');
    }
}
