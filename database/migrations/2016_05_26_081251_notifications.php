<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('people_id')->unsigned();
            $table->string('msg',100);
            $table->tinyinteger('is_seen')->default(0)->unsigned();
            $table->timestamps();
        });

        Schema::table('notifications', function($table)
        {
            $table->foreign('people_id')->references('id')->on('peoples')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
