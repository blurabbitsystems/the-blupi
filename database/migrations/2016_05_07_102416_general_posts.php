<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GeneralPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_posts', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('people_id')->unsigned();
            $table->string('post_body');
            $table->tinyinteger('scope_level')->default(0)->unsigned();
            $table->tinyinteger('scope_term')->default(0)->unsigned();
            $table->tinyinteger('comment_disabled')->default(0)->unsigned();
            $table->timestamps();
        });

        Schema::table('general_posts', function($table)
        {
            $table->foreign('people_id')->references('id')->on('peoples')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('general_posts');
    }
}
