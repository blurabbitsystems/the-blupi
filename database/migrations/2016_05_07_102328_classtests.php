<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Classtests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classtests', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('circle_id')->unsigned();
            $table->string('syllabus',150);
            $table->timestamp('schedule');
            $table->binary('evaluation')->nullable();
            $table->timestamps();
        });

        Schema::table('classtests', function($table)
        {
            $table->foreign('circle_id')->references('id')->on('circles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('classtests');
    }
}
