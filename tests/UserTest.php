<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Session;
use Auth;
use App\Models\Message;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginPage()
    {
        $this->visit('/admin')->see('Login:');

        $response = $this->call('post', '/login',
	    [
	        'username' => 'diego',
	        'password' => 'wrong',
	        '_token' => csrf_token()
	    ]);

	    $this->visit('/')->see('Invalid credentials.');
    }

    public function testHomePage()
    {
    	Session::start();

	    $response = $this->call('post', '/login',
	    [
	        'username' => 'disha',
	        'password' => 'disha',
	        '_token' => csrf_token()
	    ]);

	    $this->visit('/')->see('Post it!');
    }

    public function testProfilePage()
    {
    	Session::start();
	    $response = $this->call('post', '/login',
	    [
	        'username' => 'disha',
	        'password' => 'disha',
	        '_token' => csrf_token()
	    ]);

	    $this->visit('/profile')->see('Disha');
    }

    public function testInboxPage()
    {
    	Session::start();
	    $response = $this->call('post', '/login',
	    [
	        'username' => 'disha',
	        'password' => 'disha',
	        '_token' => csrf_token()
	    ]);

	    $this->visit('/inbox')->see('New message');
    }
}
