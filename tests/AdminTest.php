<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testLoginPage()
    {
        $this->visit('/admin')
        	 ->see('Admin Login:');
    }

    public function testAuthentication()
    {
    	$this->visit('/admin')
	         ->type('root', 'username')
	         ->type('root', 'password')
	         ->press('Login')
	         ->seePageIs('/admin/home');
    }
}
