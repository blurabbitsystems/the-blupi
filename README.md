# **The BluPi**
![blupi-logo-shadowed.png](https://bitbucket.org/repo/xooXjE/images/77511619-blupi-logo-shadowed.png)

## **Introduction**
* BluPi takes after currently available social networking systems.
* But it is to be designed to achieve a different goal.
* The main objective is to provide an improved communication
platform for academic purpose.
* Hence it can be thought of an university management system
with a few networking tweaks.

## **Contributors**

* aswasif007
* mayisha

## **Installation Procedure**

1. Configure a database in config/database.php file in *the-blupi* directory.
2. Create a new database schema in your server. Name it according to the configuration above.
3. Go back to *the-blupi* directory using terminal and run 'php artisan migrate'. Database schema will be created by now.
4. Now run 'php artisan serve'. System should be up and running. To check, open browser and go to '<host>/login'.
5. A superuser 'root' will be available by default. To login as 'root', go to '<host>/admin/login' and provide 'root' as both username and password.

### ©2016